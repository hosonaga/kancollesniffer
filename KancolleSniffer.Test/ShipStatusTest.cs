﻿// Copyright (C) 2013-2021 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class ShipStatusTest
    {
        private static AdditionalData additionalData = new AdditionalData().LoadMinAsw().LoadTbBonus();

        private static readonly ItemStatus 三式水中探信儀 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 47,
                Name = "三式水中探信儀",
                Type = 14,
                Asw = 10
            }
        };

        private static readonly ItemStatus 三式爆雷投射機 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 45,
                Name = "三式爆雷投射機",
                Type = 15,
                Asw = 8
            }
        };

        private static readonly ItemStatus 九五式爆雷 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 226,
                Name = "九五式爆雷",
                Type = 15,
                Asw = 4
            }
        };

        // ReSharper disable once InconsistentNaming
        private static readonly ItemStatus SGレーダー初期型 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 315,
                Name = "SG レーダー(初期型)",
                Type = 12,
                LoS = 8,
                Asw = 3
            }
        };

        // ReSharper disable once InconsistentNaming
        private static readonly ItemStatus SKレーダー = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 278,
                Name = "SK レーダー",
                Type = 13,
                LoS = 10
            }
        };

        // ReSharper disable once InconsistentNaming
        private static readonly ItemStatus 二式12cm迫撃砲改 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 346,
                Name = "二式12cm迫撃砲改",
                Type = 15,
                Asw = 3
            }
        };

        private static ItemStatus 流星改(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 52,
                    Name = "流星改",
                    Type = 8,
                    IconType = 8,
                    SubType = 33,
                    Torpedo = 13,
                    Asw = 3
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus カ号観測機(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 69,
                    Name = "カ号観測機",
                    Type = 25,
                    Asw = 9
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus 三式指揮連絡機対潜(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 70,
                    Name = "三式指揮連絡機(対潜)",
                    Type = 26,
                    Asw = 7
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus 九九式艦爆(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 23,
                    Name = "九九式艦爆",
                    Type = 7,
                    Asw = 3
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static readonly ItemStatus 水中聴音機零式 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 132,
                Name = "水中聴音機零式",
                Type = 40,
                Asw = 11
            }
        };

        private static ItemStatus 九七式艦攻九三一空(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 82,
                    Name = "九七式艦攻(九三一空)",
                    Type = 8,
                    Asw = 7,
                    Torpedo = 6
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus Swordfish(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 242,
                    Name = "Swordfish",
                    Type = 8,
                    IconType = 8,
                    SubType = 28,
                    Firepower = 2,
                    Torpedo = 3,
                    Asw = 4
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus TBF(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 256,
                    Name = "TBF",
                    Type = 8,
                    Asw = 6,
                    Torpedo = 9
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus Ju87C改二KMX搭載機(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 305,
                    Name = "Ju87C改二(KMX搭載機)",
                    Type = 7,
                    Bomber = 9,
                    Asw = 9
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus 橘花改(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 200,
                    Name = "橘花改",
                    Type = 57,
                    Bomber = 11,
                    Asw = 0
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus 天山一二型甲(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 372,
                    Name = "天山一二型甲",
                    Type = 8,
                    Torpedo = 9,
                    Asw = 4
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus 天山一二型甲改空六号電探改装備機(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 373,
                    Name = "天山一二型甲改(空六号電探改装備機)",
                    Type = 8,
                    IconType = 46,
                    Torpedo = 11,
                    Asw = 6
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus BarracudaMkII(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 424,
                    Name = "Barracuda Mk.II",
                    Type = 8,
                    Torpedo = 7,
                    Asw = 5
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus 瑞雲(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 26,
                    Name = "瑞雲",
                    Type = 11,
                    Bomber = 4,
                    Asw = 4
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus 瑞雲六三四空(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 79,
                    Name = "瑞雲(六三四空)",
                    Type = 11,
                    Bomber = 6,
                    Asw = 5
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus 瑞雲12型(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 80,
                    Name = "瑞雲12型",
                    Type = 11,
                    Bomber = 7,
                    Asw = 5
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus SwordfishMkIII改水上機型(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 368,
                    Name = "Swordfish Mk.III改(水上機型)",
                    Type = 11,
                    Bomber = 7,
                    Torpedo = 6,
                    Asw = 7
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static readonly ItemStatus Re2001G改 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 188,
                Name = "Re.2001 G改",
                Type = 8,
                Asw = 0,
                Torpedo = 4
            },
            OnSlot = 1,
            MaxEq = 1
        };

        private static readonly ItemStatus 彗星一二型三一号光電管爆弾搭載機 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 320,
                Name = "彗星一二型(三一号光電管爆弾搭載機)",
                Type = 7,
                Asw = 0,
                Bomber = 11
            },
            OnSlot = 1,
            MaxEq = 1
        };

        private static ItemStatus S51J(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 326,
                    Name = "S-51J",
                    Type = 25,
                    SubType2 = 44,
                    Asw = 12
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus S51J改(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 327,
                    Name = "S-51J改",
                    Type = 25,
                    SubType2 = 44,
                    Asw = 13
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static readonly ItemStatus _15_5cm三連装副砲 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 12,
                Name = "15.5cm三連装副砲",
                Type = 4,
                IconType = 4,
                Firepower = 7
            }
        };

        private static readonly ItemStatus _61cm三連装魚雷 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 13,
                Name = "61cm三連装魚雷",
                Type = 5,
                IconType = 5,
                Torpedo = 5
            }
        };

        private static readonly ItemStatus 甲標的甲型 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 41,
                Name = "甲標的 甲型",
                Type = 22,
                IconType = 5,
                Torpedo = 12
            }
        };

        [TestClass]
        public class OpeningSubmarineAttack
        {
            /// <summary>
            /// 通常の先制対潜
            /// </summary>
            [TestMethod]
            public void CheckStandardCase()
            {
                var ship = new ShipStatus
                {
                    Level = 99,
                    MaxAsw = 2,
                    Spec = new ShipSpec {ShipType = 3, GetMinAsw = (Func<int>)(() => 1)},
                    Slot = new[] {三式水中探信儀},
                    ShownAsw = 99
                };
                Assert.IsFalse(ship.EnableOpeningAsw, "対潜不足");
                ship.ShownAsw = 100;
                Assert.IsTrue(ship.EnableOpeningAsw);
                ship.Slot = new[] {new ItemStatus()};
                Assert.IsFalse(ship.EnableOpeningAsw, "ソナー未搭載");
            }


            /// <summary>
            /// 海防艦の先制対潜
            /// </summary>
            [TestMethod]
            public void CheckCoastGuard()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec {ShipType = 1},
                    Slot = new[] {九五式爆雷},
                    ShownAsw = 74
                };
                Assert.IsFalse(ship.EnableOpeningAsw, "対潜不足");

                ship.ShownAsw = 75;
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]{SGレーダー初期型};
                Assert.IsFalse(ship.EnableOpeningAsw, "装備対潜不足");

                ship.Slot = new[]{SGレーダー初期型, SGレーダー初期型};
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            /// <summary>
            /// 無条件で先制対潜が可能
            /// </summary>
            [DataTestMethod]
            [DataRow(141, "五十鈴改二", "いすず", 20, 10096)]
            [DataRow(478, "龍田改二", "たつた", 21, 10016)]
            [DataRow(394, "Jervis改", "ジャーヴィス", 82, 33512)]
            [DataRow(893, "Janus改", "ジェーナス", 82, 33522)]
            [DataRow(681, "Samuel B.Roberts改", "サミュエル・B・ロバーツ", 87, 32612)]
            [DataRow(562, "Johnston", "ジョンストン", 91, 32551)]
            [DataRow(689, "Johnston改", "ジョンストン", 91, 32552)]
            [DataRow(596, "Fletcher", "フレッチャー", 91, 32511)]
            [DataRow(692, "Fletcher改", "フレッチャー", 91, 32512)]
            [DataRow(624, "夕張改二丁", "ゆうばり", 34, 10178)]
            [DataRow(628, "Fletcher改 Mod.2", "フレッチャー", 91, 32513)]
            [DataRow(629, "Fletcher Mk.II", "フレッチャー", 91, 32516)]
            public void CheckNonConditional(int id, string name, string yomi, int klass, int sort_id)
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec
                    {
                        Id = id,
                        Name = name,
                        Yomi = yomi,
                        ShipClass = klass,
                        SortId = sort_id
                    }
                };
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            /// <summary>
            /// 未改は無条件先制対潜が不可
            /// </summary>
            [DataTestMethod]
            [DataRow(519, "Jervis", "ジャーヴィス", 82, 33511)]
            [DataRow(520, "Janus", "ジェーナス", 82, 33521)]
            [DataRow(561, "Samuel B.Roberts", "サミュエル・B・ロバーツ", 87, 32611)]
            public void CheckFalseNonConditional(int id, string name, string yomi, int klass, int sort_id)
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec
                    {
                        Id = id,
                        Name = name,
                        Yomi = yomi,
                        ShipClass = klass,
                        SortId = sort_id
                    }
                };
                Assert.IsFalse(ship.EnableOpeningAsw);
            }

            /// <summary>
            /// 大鷹改・改二、雲鷹改・改二、神鷹改・改二、加賀改二護
            /// </summary>
            [DataTestMethod]
            [DataRow(380, "大鷹改", "たいよう", 76, 65)]
            [DataRow(529, "大鷹改二", "たいよう", 76, 75)]
            [DataRow(382, "雲鷹改", "うんよう", 76, 64)]
            [DataRow(889, "雲鷹改二", "しんよう", 76, 74)]
            [DataRow(381, "神鷹改", "しんよう", 76, 66)]
            [DataRow(536, "神鷹改二", "しんよう", 76, 73)]
            [DataRow(646, "加賀改二護", "かが", 3, 0)]
            public void CheckAntiSubmarineAircraftCarrier(int id, string name, string yomi, int klass, int asw)
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec
                    {
                        Id = id,
                        Name = name,
                        Yomi = yomi,
                        ShipClass = klass,
                        GetMinAsw = (Func<int>)(() => asw)
                    },
                    Slot = new ItemStatus[0]
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {流星改(0)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {流星改(1)};
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[] {カ号観測機(0)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {カ号観測機(1)};
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[] {三式指揮連絡機対潜(0)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {三式指揮連絡機対潜(1)};
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[] {九九式艦爆(0)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {九九式艦爆(1)};
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[] {Re2001G改};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {彗星一二型三一号光電管爆弾搭載機};
                Assert.IsFalse(ship.EnableOpeningAsw);
            }

            /// <summary>
            /// 春日丸、大鷹、八幡丸、雲鷹、神鷹
            /// </summary>
            [DataTestMethod]
            [DataRow(521, "春日丸", "たいよう", 75, 0)]
            [DataRow(526, "大鷹", "たいよう", 76, 35)]
            [DataRow(522, "八幡丸", "たいよう", 75, 0)]
            [DataRow(884, "雲鷹", "たいよう", 76, 34)]
            [DataRow(534, "神鷹", "しんよう", 76, 36)]
            public void CheckFalseAntiSubmarineAircraftCarrier(int id, string name, string yomi, int klass, int asw)
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec
                    {
                        Id = id,
                        Name = name,
                        Yomi = yomi,
                        ShipClass = klass,
                        GetMinAsw = (Func<int>)(() => asw)
                    }
                };

                ship.Slot = new[] {流星改(1)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {カ号観測機(1)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {三式指揮連絡機対潜(1)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {九九式艦爆(1)};
                Assert.IsFalse(ship.EnableOpeningAsw);
            }

            [DataTestMethod]
            public void CheckLightAircraftCarrierLevel50()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec
                    {
                        ShipType = 7
                    },
                    Slot = new ItemStatus[0]
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 49;
                ship.Slot = new[]
                {
                    水中聴音機零式,
                    九七式艦攻九三一空(1)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 50;
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    九七式艦攻九三一空(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    九七式艦攻九三一空(0),
                    Re2001G改
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    九七式艦攻九三一空(0),
                    TBF(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    TBF(1)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    三式指揮連絡機対潜(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    三式指揮連絡機対潜(0),
                    Re2001G改
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    三式指揮連絡機対潜(0),
                    TBF(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    三式指揮連絡機対潜(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    カ号観測機(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    カ号観測機(0),
                    Re2001G改
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    カ号観測機(0),
                    TBF(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    カ号観測機(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            [DataTestMethod]
            public void CheckLightAircraftCarrierLevel65()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec
                    {
                        ShipType = 7
                    },
                    Slot = new ItemStatus[0]
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 64;
                ship.Slot = new[]
                {
                    九七式艦攻九三一空(1)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 65;
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    九七式艦攻九三一空(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    九七式艦攻九三一空(0),
                    Re2001G改
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    九七式艦攻九三一空(0),
                    TBF(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    TBF(1)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    カ号観測機(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    カ号観測機(0),
                    Re2001G改
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    カ号観測機(0),
                    TBF(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    カ号観測機(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式指揮連絡機対潜(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式指揮連絡機対潜(0),
                    Re2001G改
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式指揮連絡機対潜(0),
                    TBF(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式指揮連絡機対潜(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            [DataTestMethod]
            public void CheckLightAircraftCarrierLevel100()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec
                    {
                        ShipType = 7
                    },
                    Slot = new ItemStatus[0]
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 100;
                ship.Slot = new[]
                {
                    水中聴音機零式,
                    カ号観測機(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    カ号観測機(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    流星改(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    流星改(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    九九式艦爆(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    九九式艦爆(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.ShownAsw = 99;
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 100;
                ship.Slot = new[]
                {
                    水中聴音機零式,
                    Re2001G改
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    水中聴音機零式,
                    彗星一二型三一号光電管爆弾搭載機
                };
                Assert.IsFalse(ship.EnableOpeningAsw);
            }

            [TestMethod]
            public void 揚陸艦()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec
                    {
                        Id = 626,
                        Name = "神州丸改",
                        ShipType = 17
                    },
                    ShownAsw = 48,
                    Slot = new ItemStatus[0]
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式水中探信儀,
                    カ号観測機(1)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式水中探信儀,
                    瑞雲(1)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.ShownAsw = 100;

                ship.Slot = new[]
                {
                    カ号観測機(1)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    瑞雲(1)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式水中探信儀
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式水中探信儀,
                    カ号観測機(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式水中探信儀,
                    カ号観測機(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式水中探信儀,
                    瑞雲(0)
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[]
                {
                    三式水中探信儀,
                    瑞雲(1)
                };
                Assert.IsTrue(ship.EnableOpeningAsw);
            }

            [TestMethod]
            public void 日向改二()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec {Id = 554, Name = "日向改二"},
                    Slot = new ItemStatus[0]
                };
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {カ号観測機(1)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {カ号観測機(1), カ号観測機(0)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {カ号観測機(1), カ号観測機(1)};
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[] {S51J(0)};
                Assert.IsFalse(ship.EnableOpeningAsw);

                ship.Slot = new[] {S51J(1)};
                Assert.IsTrue(ship.EnableOpeningAsw);

                ship.Slot = new[] {S51J改(1)};
                Assert.IsTrue(ship.EnableOpeningAsw);
            }
        }

        private static ItemStatus TBM3D(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 257,
                    Name = "TBM-3D",
                    Type = 8,
                    IconType = 46,
                    SubType = 21,
                    Firepower = 2,
                    Torpedo = 9,
                    AntiAir = 1,
                    Asw = 8
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static ItemStatus F6F3N(int onSlot)
        {
            return new ItemStatus
            {
                Id = 1,
                Spec = new ItemSpec
                {
                    Id = 254,
                    Name = "F6F-3N",
                    Type = 6,
                    IconType = 45,
                    SubType = 21,
                    AntiAir = 8,
                    Asw = 4
                },
                OnSlot = onSlot,
                MaxEq = onSlot
            };
        }

        private static readonly ItemStatus 夜間作戦航空要員 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 259,
                Name = "夜間作戦航空要員+熟練甲板員",
                Firepower = 3
            }
        };

        private static readonly Fleet NormalFleet = new Fleet(null, 0, null)
        {
            CombinedType = CombinedType.None
        };

        [TestClass]
        public class DayFirepower
        {
            [TestMethod]
            public void 軽巡に軽巡砲を装備して火力補正()
            {
                var ship = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Firepower = 21,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec {Id = 52, ShipType = 3, MinFirepower = 11},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 6,
                                Name = "20.3cm連装砲",
                                Firepower = 8
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 4,
                                Name = "14cm単装砲",
                                Firepower = 2
                            }
                        }
                    }
                };
                Assert.AreEqual(27, ship.EffectiveFirepower);

                var ship2 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Firepower = 21,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec {Id = 58, ShipType = 4, MinFirepower = 8},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 6,
                                Name = "20.3cm連装砲",
                                Firepower = 8
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 65,
                                Name = "15.2cm連装砲",
                                Firepower = 5
                            }
                        }
                    }
                };
                Assert.AreEqual(28, ship2.EffectiveFirepower);

                var ship3 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Firepower = 30,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec {Id = 343, ShipType = 21, MinFirepower = 16},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 4,
                                Name = "14cm単装砲",
                                Firepower = 2
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 65,
                                Name = "15.2cm連装砲",
                                Firepower = 5
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 11,
                                Name = "15.2cm単装砲",
                                Firepower = 2
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 359,
                                Name = "6inch 連装速射砲 Mk.XXI",
                                Firepower = 5
                            }
                        }
                    }
                };
                Assert.AreEqual(3924, (int)(ship3.EffectiveFirepower * 100));

                var ship4 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Firepower = 50,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec {Id = 262, ShipType = 5, MinFirepower = 36},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 4,
                                Name = "14cm単装砲",
                                Firepower = 2
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 65,
                                Name = "15.2cm連装砲",
                                Firepower = 5
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 11,
                                Name = "15.2cm単装砲",
                                Firepower = 2
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 359,
                                Name = "6inch 連装速射砲 Mk.XXI",
                                Firepower = 5
                            }
                        }
                    }
                };
                Assert.AreEqual(55, ship4.EffectiveFirepower);
            }

            [TestMethod]
            public void イタリア重巡にイタリア重巡砲を装備して火力補正()
            {
                var ship = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Firepower = 53,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec {Id = 448, ShipType = 5, ShipClass = 64, MinFirepower = 36},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 6,
                                Name = "20.3cm連装砲",
                                Firepower = 8
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 162,
                                Name = "203mm/53 連装砲",
                                Firepower = 9
                            }
                        }
                    }
                };
                Assert.AreEqual(59, ship.EffectiveFirepower);

                var ship2 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Firepower = 59,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec {Id = 361, ShipType = 5, ShipClass = 64, MinFirepower = 41},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 162,
                                Name = "203mm/53 連装砲",
                                Firepower = 9
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 162,
                                Name = "203mm/53 連装砲",
                                Firepower = 9
                            }
                        }
                    }
                };
                Assert.AreEqual(6541, (int)(ship2.EffectiveFirepower * 100));

                var ship3 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Firepower = 48,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec {Id = 59, ShipType = 5, ShipClass = 7, MinFirepower = 30},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 162,
                                Name = "203mm/53 連装砲",
                                Firepower = 9
                            }
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 162,
                                Name = "203mm/53 連装砲",
                                Firepower = 9
                            }
                        }
                    }
                };
                Assert.AreEqual(53, ship3.EffectiveFirepower);
            }

            [TestMethod]
            public void 空母に艦攻0機で昼攻撃不可()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68,
                    Torpedo = 0,
                    Spec = new ShipSpec {ShipType = 11}
                };
                ship.Slot = new[] {流星改(0)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 空母に艦爆0機で昼攻撃不可()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68,
                    Torpedo = 0,
                    Spec = new ShipSpec {ShipType = 11}
                };
                ship.Slot = new[] {九九式艦爆(0)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 速吸改に艦攻装備で空母と同じ昼爆撃()
            {
                var ship = new ShipStatus
                {
                    Firepower = 36,
                    Torpedo = 13,
                    Spec = new ShipSpec
                    {
                        Id = 352,
                        ShipType = 22,
                        ShipClass = 60,
                        Name = "速吸改",
                        Yomi = "はやすい",
                        MinFirepower = 8
                    },
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {流星改(6)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(128, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 速吸改に艦攻と水上機装備で水上機の攻撃力が加算()
            {
                var ship = new ShipStatus
                {
                    Firepower = 36,
                    Torpedo = 13,
                    Spec = new ShipSpec
                    {
                        Id = 352,
                        ShipType = 22,
                        ShipClass = 60,
                        Name = "速吸改",
                        Yomi = "はやすい",
                        MinFirepower = 8
                    },
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {流星改(6), 瑞雲(1)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(136, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 速吸改に艦攻0機で昼攻撃不可()
            {
                var ship = new ShipStatus
                {
                    Firepower = 36,
                    Torpedo = 13,
                    Spec = new ShipSpec
                    {
                        Id = 352,
                        ShipType = 22,
                        ShipClass = 60,
                        Name = "速吸改",
                        Yomi = "はやすい",
                        MinFirepower = 8
                    },
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {流星改(0)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 速吸改に艦攻0機と水上機でも昼攻撃不可()
            {
                var ship = new ShipStatus
                {
                    Firepower = 36,
                    Torpedo = 13,
                    Spec = new ShipSpec
                    {
                        Id = 352,
                        ShipType = 22,
                        ShipClass = 60,
                        Name = "速吸改",
                        Yomi = "はやすい",
                        MinFirepower = 8
                    },
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {流星改(0), 瑞雲(1)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.EffectiveFirepower);
            }

            [TestMethod]
            public void 不明な空母には雷装ボーナスなし()
            {
                var ship = new ShipStatus
                {
                    Firepower = 63,
                    Torpedo = 0,
                    Spec = new ShipSpec {ShipType = 11},
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {天山一二型甲(27)};
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(163, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦につかない");
                Assert.AreEqual(0, ship.Slot[0].TbBonus, "天山に雷装ボーナスがつかない");
                CollectionAssert.AreEqual(new int[] {57, 107}, ship.Slot[0].CalcBomberPower(), "天山の雷装ボーナスが航空戦につかない");
            }

            [TestMethod]
            public void 翔鶴改二の天山に雷装ボーナス()
            {
                var ship = new ShipStatus
                {
                    Firepower = 63 + 1,
                    Torpedo = 0,
                    Spec = new ShipSpec {Id = 461, ShipType = 11, Name = "翔鶴改二"},
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {天山一二型甲(27)};
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(166, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(1, ship.Slot[0].TbBonus, "天山に雷装ボーナスがつく");
                CollectionAssert.AreEqual(new int[] {61, 115}, ship.Slot[0].CalcBomberPower(), "天山の雷装ボーナスが航空戦につく");
            }

            [TestMethod]
            public void 翔鶴改二の流星改に雷装ボーナス()
            {
                var ship = new ShipStatus
                {
                    Firepower = 63 + 1,
                    Torpedo = 0,
                    Spec = new ShipSpec {Id = 461, ShipType = 11, Name = "翔鶴改二"},
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {天山一二型甲(27), 流星改(27)};
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(185, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(0, ship.Slot[0].TbBonus, "天山の雷装ボーナスは天山につかない");
                CollectionAssert.AreEqual(new int[] {57, 107}, ship.Slot[0].CalcBomberPower(), "雷装ボーナスは天山の航空戦につかない");
                Assert.AreEqual(1, ship.Slot[1].TbBonus, "天山の雷装ボーナスは流星改につく");
                CollectionAssert.AreEqual(new int[] {78, 146}, ship.Slot[1].CalcBomberPower(), "雷装ボーナスは流星改の航空戦につく");
            }

            [TestMethod]
            public void 翔鶴改二の搭載0の流星改には雷装ボーナスつかない()
            {
                var ship = new ShipStatus
                {
                    Firepower = 63 + 1,
                    Torpedo = 0,
                    Spec = new ShipSpec {Id = 461, ShipType = 11, Name = "翔鶴改二"},
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {天山一二型甲(27), 流星改(27)};
                ship.Slot[1].OnSlot = 0;
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(185, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(1, ship.Slot[0].TbBonus, "天山の雷装ボーナスは天山につく");
                CollectionAssert.AreEqual(new int[] {61, 115}, ship.Slot[0].CalcBomberPower(), "雷装ボーナスは天山の航空戦につく");
                Assert.AreEqual(0, ship.Slot[1].TbBonus, "天山の雷装ボーナスは流星改につかない");
                CollectionAssert.AreEqual(new int[] {0}, ship.Slot[1].CalcBomberPower(), "雷装ボーナスは流星改の航空戦につかない");
            }

            [TestMethod]
            public void 翔鶴改二の機数が多いJu87Cに雷装ボーナス()
            {
                var ship = new ShipStatus
                {
                    Firepower = 63 + 1,
                    Torpedo = 0,
                    Spec = new ShipSpec {Id = 461, ShipType = 11, Name = "翔鶴改二"},
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {夜間作戦航空要員, 天山一二型甲(27), Ju87C改二KMX搭載機(27)};
                ship.Slot[1].OnSlot--;
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(182, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(0, ship.Slot[1].TbBonus, "天山の雷装ボーナスは天山につかない");
                CollectionAssert.AreEqual(new int[] {56, 106}, ship.Slot[1].CalcBomberPower(), "雷装ボーナスは天山の航空戦につかない");
                Assert.AreEqual(1, ship.Slot[2].TbBonus, "天山の雷装ボーナスはJu87Cにつく");
                CollectionAssert.AreEqual(new int[] {76}, ship.Slot[2].CalcBomberPower(), "雷装ボーナスはJu87Cの航空戦につく");
            }

            [TestMethod]
            public void 翔鶴改二のスロットが上にある橘花改に雷装ボーナス()
            {
                var ship = new ShipStatus
                {
                    Firepower = 63 + 2,
                    Torpedo = 0,
                    Spec = new ShipSpec {Id = 461, ShipType = 11, Name = "翔鶴改二"},
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {夜間作戦航空要員, 橘花改(27), 天山一二型甲改空六号電探改装備機(27)};
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(193, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(2, ship.Slot[1].TbBonus, "夜攻天山の雷装ボーナスは橘花改につく");
                CollectionAssert.AreEqual(new int[] {82, 65}, ship.Slot[1].CalcBomberPower(), "雷装ボーナスは通常の航空戦のみ");
                Assert.AreEqual(0, ship.Slot[2].TbBonus, "夜攻天山の雷装ボーナスは夜攻天山につかない");
                CollectionAssert.AreEqual(new int[] {65, 123}, ship.Slot[2].CalcBomberPower(), "雷装ボーナスは夜攻天山の航空戦につかない");
            }

            [TestMethod]
            public void 翔鶴改二の夜攻天山に雷装ボーナス()
            {
                var ship = new ShipStatus
                {
                    Firepower = 63 + 2 + 1,
                    Torpedo = 0,
                    Spec = new ShipSpec {Id = 461, ShipType = 11, Name = "翔鶴改二"},
                    Fleet = NormalFleet
                };
                ship.Slot = new[] {天山一二型甲(27), 天山一二型甲改空六号電探改装備機(27)};
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(185, ship.EffectiveFirepower, "雷装ボーナスは砲撃戦にもつく");
                Assert.AreEqual(0, ship.Slot[0].TbBonus, "天山の雷装ボーナスは天山につかない");
                CollectionAssert.AreEqual(new int[] {57, 107}, ship.Slot[0].CalcBomberPower(), "雷装ボーナスは天山の航空戦につかない");
                Assert.AreEqual(1, ship.Slot[1].TbBonus, "天山の雷装ボーナスは夜攻天山につく");
                CollectionAssert.AreEqual(new int[] {69, 131}, ship.Slot[1].CalcBomberPower(), "雷装ボーナスは夜攻天山の航空戦につく");
            }

            [TestMethod]
            public void GotlandのSwordfishは雷装ボーナスなし()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec {Id = 574, ShipType = 3, Name = "Gotland"}
                };
                ship.Slot = new[] {SwordfishMkIII改水上機型(2)};
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(0, ship.Slot[0].TbBonus, "Swordfishに雷装ボーナスがつかない");
                CollectionAssert.AreEqual(new int[] {34}, ship.Slot[0].CalcBomberPower(), "Swordfishの爆装ボーナスが航空戦につかない");
            }

            [TestMethod]
            public void Gotland改二のSwordfishに雷装ボーナス()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec {Id = 630, ShipType = 3, Name = "Gotland andra"}
                };
                ship.Slot = new[] {SwordfishMkIII改水上機型(2)};
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(2, ship.Slot[0].TbBonus, "Swordfishに雷装ボーナスがつく");
                CollectionAssert.AreEqual(new int[] {37}, ship.Slot[0].CalcBomberPower(), "Swordfishの爆装ボーナスが航空戦につく");
            }

            [TestMethod]
            public void Gotland改二の雷装ボーナス付加は爆装で判定()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec {Id = 630, ShipType = 3, Name = "Gotland andra"}
                };
                ship.Slot = new[] {瑞雲六三四空(2), SwordfishMkIII改水上機型(2)};
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(0, ship.Slot[0].TbBonus, "Swordfishの雷装ボーナスは瑞雲につかない");
                CollectionAssert.AreEqual(new int[] {33}, ship.Slot[0].CalcBomberPower(), "雷装ボーナスは瑞雲の航空戦につかない");
                Assert.AreEqual(2, ship.Slot[1].TbBonus, "Swordfishの雷装ボーナスはSwordfishにつく");
                CollectionAssert.AreEqual(new int[] {37}, ship.Slot[1].CalcBomberPower(), "雷装ボーナスはSwordfishの航空戦につく");
            }

            [TestMethod]
            public void Gotland改二の瑞雲に雷装ボーナス()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec {Id = 630, ShipType = 3, Name = "Gotland andra"}
                };
                ship.Slot = new[] {瑞雲12型(2), SwordfishMkIII改水上機型(2)};
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(2, ship.Slot[0].TbBonus, "Swordfishの雷装ボーナスは瑞雲につく");
                CollectionAssert.AreEqual(new int[] {37}, ship.Slot[0].CalcBomberPower(), "雷装ボーナスは瑞雲の航空戦につく");
                Assert.AreEqual(0, ship.Slot[1].TbBonus, "Swordfishの雷装ボーナスはSwordfishにつかない");
                CollectionAssert.AreEqual(new int[] {34}, ship.Slot[1].CalcBomberPower(), "雷装ボーナスはSwordfishの航空戦につかない");
            }
        }

        [TestClass]
        public class TorpedoEnabled
        {
            [TestMethod]
            public void 通常の駆逐軽巡重順は初期状態で雷撃可能()
            {
                var ship = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Torpedo = 18,
                    ImprovedTorpedo = 0,
                    Spec = new ShipSpec {Id = 1, ShipType = 2, MinTorpedo = 18}
                };
                Assert.AreEqual(23, ship.EffectiveTorpedo);

                var ship2 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Torpedo = 18,
                    ImprovedTorpedo = 0,
                    Spec = new ShipSpec {Id = 51, ShipType = 3, MinTorpedo = 18}
                };
                Assert.AreEqual(23, ship2.EffectiveTorpedo);

                var ship3 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Torpedo = 12,
                    ImprovedTorpedo = 0,
                    Spec = new ShipSpec {Id = 59, ShipType = 5, MinTorpedo = 12}
                };
                Assert.AreEqual(17, ship3.EffectiveTorpedo);
            }

            [TestMethod]
            public void 雷装初期値0のままでは雷撃不可()
            {
                var ship = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Torpedo = 0,
                    ImprovedTorpedo = 0,
                    Spec = new ShipSpec {Id = 183, ShipType = 3, MinTorpedo = 0}
                };
                Assert.AreEqual(0, ship.EffectiveTorpedo);

                ship.Torpedo = _61cm三連装魚雷.Spec.Torpedo;
                ship.Slot = new[] {_61cm三連装魚雷};
                Assert.AreEqual(0, ship.EffectiveTorpedo);

                ship.Torpedo = 1;
                ship.ImprovedTorpedo = 1;
                ship.Slot = new ItemStatus[0];
                Assert.AreEqual(6, ship.EffectiveTorpedo);
            }

            [TestMethod]
            public void 空母水母速吸改神威改はどんな装備でも雷撃不可()
            {
                var ship = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Torpedo = 13,
                    ImprovedTorpedo = 0,
                    Spec = new ShipSpec {Id = 89, ShipType = 7, MinTorpedo = 0},
                    Slot = new[] {流星改(8)}
                };
                Assert.AreEqual(0, ship.EffectiveTorpedo);

                var ship2 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Torpedo = 13,
                    ImprovedTorpedo = 0,
                    Spec = new ShipSpec {Id = 352, ShipType = 22, MinTorpedo = 0},
                    Slot = new[] {流星改(6)}
                };
                Assert.AreEqual(0, ship2.EffectiveTorpedo);

                var ship3 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Torpedo = 12,
                    ImprovedTorpedo = 0,
                    Spec = new ShipSpec {Id = 499, ShipType = 16, MinTorpedo = 0},
                    Slot = new[] {甲標的甲型}
                };
                Assert.AreEqual(0, ship3.EffectiveTorpedo);

                var ship4 = new ShipStatus
                {
                    Fleet = NormalFleet,
                    Torpedo = 4,
                    ImprovedTorpedo = 0,
                    Spec = new ShipSpec {Id = 451, ShipType = 16, MinTorpedo = 0},
                    Slot = new[] {瑞雲(12)}
                };
                Assert.AreEqual(0, ship4.EffectiveTorpedo);
            }
        }

        [TestClass]
        public class NightBattlePower
        {
            [TestMethod]
            public void 甲標的の改修効果()
            {
                var ship = new ShipStatus
                {
                    Torpedo = 102,
                    Spec = new ShipSpec
                    {
                        ShipType = 4,
                        MinFirepower = 8,
                        MinTorpedo = 80
                    },
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 309,
                                Name = "甲標的 丙型",
                                Torpedo = 14,
                                Type = 22
                            },
                            Level = 4
                        }
                    }
                };
                Assert.AreEqual(104, ship.NightBattlePower);
            }

            [TestMethod]
            public void LuigiTorelli改は夜戦不可()
            {
                var ship = new ShipStatus
                {
                    Firepower = 8,
                    Torpedo = 32,
                    Spec = new ShipSpec
                    {
                        Id = 605,
                        ShipType = 13,
                        ShipClass = 80,
                        Name = "Luigi Torelli改",
                        Yomi = "ルイージ・トレッリ",
                        MinFirepower = 0,
                        MinTorpedo = 0
                    }
                };
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void 宗谷特務は夜戦可能()
            {
                var ship = new ShipStatus
                {
                    Firepower = 9,
                    Torpedo = 0,
                    Spec = new ShipSpec
                    {
                        Id = 699,
                        ShipType = 22,
                        ShipClass = 111,
                        Name = "宗谷",
                        Yomi = "そうや",
                        MinFirepower = 1,
                        MinTorpedo = 0
                    }
                };
                Assert.AreEqual(9, ship.NightBattlePower);
            }

            [TestMethod]
            public void 宗谷灯台南極は夜戦不可()
            {
                var ship = new ShipStatus
                {
                    Firepower = 1,
                    Torpedo = 0,
                    Spec = new ShipSpec
                    {
                        Id = 645,
                        ShipType = 22,
                        ShipClass = 111,
                        Name = "宗谷",
                        Yomi = "そうや",
                        MinFirepower = 0,
                        MinTorpedo = 0
                    }
                };
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void 夜戦不可空母()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68 + 2,
                    Torpedo = 9,
                    Spec = new ShipSpec {ShipType = 11}
                };
                ship.Slot = new[] {TBM3D(32), F6F3N(24)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void 速吸改に夜戦装備しても砲撃()
            {
                var ship = new ShipStatus
                {
                    Firepower = 36 + 2,
                    Torpedo = 9,
                    Spec = new ShipSpec
                    {
                        Id = 352,
                        ShipType = 22,
                        ShipClass = 60,
                        Name = "速吸改",
                        Yomi = "はやすい",
                        MinFirepower = 8
                    }
                };
                ship.Slot = new[] {TBM3D(6), 夜間作戦航空要員};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(47, ship.NightBattlePower);
            }

            [TestMethod]
            public void 日向改二に夜戦装備しても砲撃()
            {
                var ship = new ShipStatus
                {
                    Firepower = 86,
                    Torpedo = 0,
                    Spec = new ShipSpec
                    {
                        Id = 554,
                        ShipType = 10,
                        ShipClass = 2,
                        Name = "日向改二",
                        Yomi = "ひゅうが",
                        MinFirepower = 58
                    }
                };
                ship.Slot = new[] {F6F3N(2), 夜間作戦航空要員};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(86, ship.NightBattlePower);
            }

            // 空母夜戦火力はwiki参考 https://wikiwiki.jp/kancolle/%E5%A4%9C%E6%88%A6#x345rgc6
            [TestMethod]
            public void サラトガmk2にTBM3D()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68 + 2,
                    Torpedo = 9,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11, Id = 545}
                };
                ship.Slot = new[] {TBM3D(32)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((68 + 2) * 100 + 9 * 100 + 14436, (int)(ship.NightBattlePower * 100));
            }

            public void サラトガmk2にTBM3Dを0機()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68 + 2,
                    Torpedo = 9,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11, Id = 545}
                };
                ship.Slot = new[] {TBM3D(0)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void サラトガmk2にF6F3N()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68,
                    Torpedo = 0,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11, Id = 545}
                };
                ship.Slot = new[] {F6F3N(32)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(68 * 100 + 10618, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void サラトガmk2にF6F3Nを0機()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68,
                    Torpedo = 0,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11, Id = 545}
                };
                ship.Slot = new[] {F6F3N(0)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void サラトガmk2にTBM3DとF6F3N()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68 + 2,
                    Torpedo = 9,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11, Id = 545}
                };
                ship.Slot = new[] {TBM3D(32), F6F3N(24)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((68 + 2) * 100 + 9 * 100 + 14436 + 8082, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void サラトガmk2にTBM3Dを0機とF6F3N()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68 + 2,
                    Torpedo = 9,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11, Id = 545}
                };
                ship.Slot = new[] {TBM3D(0), F6F3N(24)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(68 * 100 + 8081, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void サラトガmk2にTBM3DとF6F3Nを0機()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68 + 2,
                    Torpedo = 9,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11, Id = 545}
                };
                ship.Slot = new[] {TBM3D(32), F6F3N(0)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((68 + 2) * 100 + 9 * 100 + 14436, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void サラトガmk2にTBM3DとF6F3Nを0機とSwordfish()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68 + 2 + 2,
                    Torpedo = 9 + 3,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11, Id = 545}
                };
                ship.Slot = new[] {TBM3D(0), F6F3N(0), Swordfish(18)};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void 夜戦不可空母に夜間作戦航空要員()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68 + 2 + 3,
                    Torpedo = 9,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11}
                };
                ship.Slot = new[] {TBM3D(32), F6F3N(24), 夜間作戦航空要員};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((68 + 2) * 100 + 9 * 100 + 14436 + 8082, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void 赤城改二戊に天山一二型甲改熟練空六号電探改装備機と零戦62型爆戦岩井隊と九九式艦爆()
            {
                var ship = new ShipStatus
                {
                    Firepower = 67,
                    Torpedo = 13,
                    ImprovedFirepower = 67,
                    Spec = new ShipSpec {ShipType = 11, Id = 599},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 374,
                                Name = "天山一二型甲改(熟練/空六号電探改装備機)",
                                Type = 8,
                                IconType = 46,
                                SubType = 1,
                                Torpedo = 13,
                                AntiAir = 1,
                                Asw = 7
                            },
                            OnSlot = 40,
                            MaxEq = 40
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 154,
                                Name = "零戦62型(爆戦/岩井隊)",
                                Type = 7,
                                IconType = 7,
                                SubType = 12,
                                Bomber = 4,
                                AntiAir = 7,
                                Asw = 3
                            },
                            OnSlot = 16,
                            MaxEq = 16,
                            Level = 10
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 23,
                                Name = "九九式艦爆",
                                Type = 7,
                                IconType = 7,
                                SubType = 15,
                                Bomber = 5,
                                Asw = 3
                            },
                            OnSlot = 4,
                            MaxEq = 4
                        }
                    }
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(67 * 100 + 13 * 100 + 17692 + 1156, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void 加賀改二戊にTBM3W3Sと彗星一二型三一号光電管爆弾搭載機と九七式艦攻()
            {
                var ship = new ShipStatus
                {
                    Firepower = 62 + 3,
                    Torpedo = 10 + 5,
                    ImprovedFirepower = 62,
                    Spec = new ShipSpec {ShipType = 11, Id = 610},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 389,
                                Name = "TBM-3W+3S",
                                Type = 8,
                                IconType = 46,
                                SubType = 39,
                                Firepower = 3,
                                Torpedo = 10,
                                Bomber = 7,
                                AntiAir = 0,
                                Asw = 13
                            },
                            OnSlot = 40,
                            MaxEq = 40
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 320,
                                Name = "彗星一二型(三一号光電管爆弾搭載機)",
                                Type = 7,
                                IconType = 7,
                                SubType = 16,
                                Bomber = 11
                            },
                            OnSlot = 18,
                            MaxEq = 18
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 16,
                                Name = "九七式艦攻",
                                Type = 8,
                                IconType = 8,
                                SubType = 1,
                                Torpedo = 5,
                                Asw = 4
                            },
                            OnSlot = 8,
                            MaxEq = 8
                        }
                    }
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((62 + 3) * 100 + (10 + 5) * 100 + 20892 + 1400, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void 龍鳳改二戊に烈風改二戊型一航戦熟練とSwordfishMkIII熟練とカ号観測機()
            {
                var ship = new ShipStatus
                {
                    Firepower = 45 + 2 + 4,
                    Torpedo = 8,
                    ImprovedFirepower = 45,
                    Spec = new ShipSpec {ShipType = 7, Id = 883},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 339,
                                Name = "烈風改二戊型(一航戦/熟練)",
                                Type = 6,
                                IconType = 45,
                                SubType = 34,
                                Firepower = 2,
                                AntiAir = 12
                            },
                            OnSlot = 21,
                            MaxEq = 21
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 244,
                                Name = "Swordfish Mk.III(熟練)",
                                Type = 8,
                                IconType = 8,
                                SubType = 28,
                                Firepower = 4,
                                Torpedo = 8,
                                Asw = 10
                            },
                            OnSlot = 12,
                            MaxEq = 12
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 69,
                                Name = "カ号観測機",
                                Type = 25,
                                IconType = 21,
                                Asw = 9
                            },
                            OnSlot = 3,
                            MaxEq = 3
                        }
                    }
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((45 + 2 + 4) * 100 + 8 * 100 + 6712 + 2286, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void サラトガmk2にTBM3W3Sと零戦62型爆戦岩井隊とSwordfishMkIII熟練とF6F5N()
            {
                var ship = new ShipStatus
                {
                    Firepower = 68 + 3 + 4,
                    Torpedo = 10 + 8,
                    ImprovedFirepower = 68,
                    Spec = new ShipSpec {ShipType = 11, Id = 545},
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 389,
                                Name = "TBM-3W+3S",
                                Type = 8,
                                IconType = 46,
                                SubType = 39,
                                Firepower = 3,
                                Torpedo = 10,
                                Bomber = 7,
                                AntiAir = 0,
                                Asw = 13
                            },
                            OnSlot = 32,
                            MaxEq = 32
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 154,
                                Name = "零戦62型(爆戦/岩井隊)",
                                Type = 7,
                                IconType = 7,
                                SubType = 12,
                                Bomber = 4,
                                AntiAir = 7,
                                Asw = 3
                            },
                            OnSlot = 24,
                            MaxEq = 24,
                            Level = 10
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 244,
                                Name = "Swordfish Mk.III(熟練)",
                                Type = 8,
                                IconType = 8,
                                SubType = 28,
                                Firepower = 4,
                                Torpedo = 8,
                                Asw = 10
                            },
                            OnSlot = 18,
                            MaxEq = 18
                        },
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 255,
                                Name = "F6F-5N",
                                Type = 6,
                                IconType = 45,
                                SubType = 21,
                                AntiAir = 10,
                                Asw = 5
                            },
                            OnSlot = 6,
                            MaxEq = 6
                        }
                    }
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((68 + 3 + 4) * 100 + (10 + 8) * 100 + 18000 + 1345 + 2800 + 2351, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void 不明な空母の夜戦に雷装ボーナスなし()
            {
                var ship = new ShipStatus
                {
                    Firepower = 63,
                    Torpedo = 0,
                    ImprovedFirepower = 63,
                    Spec = new ShipSpec {ShipType = 11}
                };
                ship.Slot = new[] {天山一二型甲改空六号電探改装備機(27), 夜間作戦航空要員};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(63 * 100 + 13175, (int)(ship.NightBattlePower * 100), "雷装ボーナスは夜戦につかない");
                Assert.AreEqual(0, ship.Slot[0].TbBonus, "夜攻天山に雷装ボーナスがつかない");
            }

            [TestMethod]
            public void 翔鶴改二の夜戦に雷装ボーナス()
            {
                var ship = new ShipStatus
                {
                    Firepower = 63,
                    Torpedo = 0,
                    ImprovedFirepower = 63,
                    Spec = new ShipSpec {Id = 461, ShipType = 11, Name = "翔鶴改二"}
                };
                ship.Slot = new[] {天山一二型甲改空六号電探改装備機(27), 夜間作戦航空要員};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((63 + 2) * 100 + 13175, (int)(ship.NightBattlePower * 100), "雷装ボーナスは夜戦にもつく");
                Assert.AreEqual(2, ship.Slot[0].TbBonus, "夜攻天山に雷装ボーナスがつく");

                ship.Slot = new[] {TBM3D(27), 天山一二型甲(27), 夜間作戦航空要員};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((63 + 1) * 100 + 13642, (int)(ship.NightBattlePower * 100), "夜攻ではない機体の雷装ボーナスが夜戦にもつく");
                Assert.AreEqual(1, ship.Slot[0].TbBonus, "天山の雷装ボーナスはTBM3Dにつく");
                Assert.AreEqual(0, ship.Slot[1].TbBonus, "天山の雷装ボーナスは天山につかない");

                ship.Slot = new[] {天山一二型甲改空六号電探改装備機(27), 流星改(27), 夜間作戦航空要員};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((63 + 2) * 100 + 13175, (int)(ship.NightBattlePower * 100), "夜攻ではない機体についた雷装ボーナスが夜戦にもつく");
                Assert.AreEqual(0, ship.Slot[0].TbBonus, "夜攻天山の雷装ボーナスは夜攻天山につかない");
                Assert.AreEqual(2, ship.Slot[1].TbBonus, "夜攻天山の雷装ボーナスは流星改につく");
            }

            [TestMethod]
            public void GambierBayMkIIに夜間作戦航空要員()
            {
                var ship = new ShipStatus
                {
                    Firepower = 55 + 2 + 3,
                    Torpedo = 9,
                    ImprovedFirepower = 52,
                    Spec = new ShipSpec
                    {
                        Id = 707,
                        ShipType = 7,
                        ShipClass = 83,
                        Name = "Gambier Bay Mk.II",
                        Yomi = "ガンビア・ベイ",
                        MinFirepower = 3
                    },
                    Slot = new[] {TBM3D(24), F6F3N(20), 夜間作戦航空要員}
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((52 + 3 + 2) * 100 + 9 * 100 + 11388 + 6805, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void GambierBayMkIIは夜戦不可()
            {
                var ship = new ShipStatus
                {
                    Firepower = 55,
                    Torpedo = 13 + 13,
                    ImprovedFirepower = 52,
                    Spec = new ShipSpec
                    {
                        Id = 707,
                        ShipType = 7,
                        ShipClass = 83,
                        Name = "Gambier Bay Mk.II",
                        Yomi = "ガンビア・ベイ",
                        MinFirepower = 3
                    },
                    Slot = new[]
                    {
                        流星改(24), 流星改(20),
                        _15_5cm三連装副砲,
                        _15_5cm三連装副砲
                    }
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void グラーフツェッペリンに夜間作戦航空要員()
            {
                var ship = new ShipStatus
                {
                    Firepower = 10 + 2 + 3,
                    Torpedo = 9,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec
                    {
                        Id = 432,
                        ShipType = 11,
                        ShipClass = 63,
                        Name = "Graf Zeppelin",
                        Yomi = "グラーフ・ツェッペリン",
                        MinFirepower = 10
                    },
                };
                ship.Slot = new[] {TBM3D(20), F6F3N(13), 夜間作戦航空要員};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((10 + 2) * 100 + 9 * 100 + 9823 + 4549, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void グラーフツェッペリンに流星改と15_5cm三連装副砲2スロずつ()
            {
                var ship = new ShipStatus
                {
                    Firepower = 10 + 7 + 7,
                    Torpedo = 13 + 13,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec
                    {
                        Id = 432,
                        ShipType = 11,
                        ShipClass = 63,
                        Name = "Graf Zeppelin",
                        Yomi = "グラーフ・ツェッペリン",
                        MinFirepower = 10
                    },
                    Slot = new[]
                    {
                        流星改(20), 流星改(13),
                        _15_5cm三連装副砲,
                        _15_5cm三連装副砲
                    }
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(50, ship.NightBattlePower);
            }

            [TestMethod]
            public void グラーフツェッペリンに流星改0機と15_5cm三連装副砲2スロずつ()
            {
                var ship = new ShipStatus
                {
                    Firepower = 10 + 7 + 7,
                    Torpedo = 13 + 13,
                    ImprovedFirepower = 0,
                    Spec = new ShipSpec
                    {
                        Id = 432,
                        ShipType = 11,
                        ShipClass = 63,
                        Name = "Graf Zeppelin",
                        Yomi = "グラーフ・ツェッペリン",
                        MinFirepower = 10
                    },
                    Slot = new[]
                    {
                        流星改(0), 流星改(0),
                        _15_5cm三連装副砲,
                        _15_5cm三連装副砲
                    }
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(50, ship.NightBattlePower);
            }

            [TestMethod]
            public void アークロイヤル改に夜間作戦航空要員()
            {
                var ship = new ShipStatus
                {
                    Firepower = 50 + 2 + 3,
                    Torpedo = 9,
                    ImprovedFirepower = 50,
                    Spec = new ShipSpec
                    {
                        Id = 393,
                        ShipType = 11,
                        ShipClass = 78,
                        Name = "Ark Royal改",
                        Yomi = "アークロイヤル",
                        MinFirepower = 0
                    }
                };
                ship.Slot = new[] {TBM3D(24), F6F3N(30), 夜間作戦航空要員};
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual((50 + 2) * 100 + 9 * 100 + 11388 + 9986, (int)(ship.NightBattlePower * 100));
            }

            [TestMethod]
            public void アークロイヤル改にSwordfishを2スロットとBarracudaMkIIと15_5cm三連装副砲()
            {
                var ship = new ShipStatus
                {
                    Firepower = 50 + 2 + 2 + 7 + 7,
                    Torpedo = 3 + 3,
                    ImprovedFirepower = 50,
                    Spec = new ShipSpec
                    {
                        Id = 393,
                        ShipType = 11,
                        ShipClass = 78,
                        Name = "Ark Royal改",
                        Yomi = "アークロイヤル",
                        MinFirepower = 0
                    },
                    Slot = new[]
                    {
                        Swordfish(24), Swordfish(30),
                        BarracudaMkII(12),
                        _15_5cm三連装副砲
                    }
                };
                ship.Slot[0].Level = 9;
                ship.Slot[2].Level = 10;
                ship.ResolveTbBonus(additionalData);

                Assert.AreEqual(0, ship.Slot[0].TbBonus, "Barracudaの雷装ボーナスはSwordfishにつかない");
                Assert.AreEqual(0, ship.Slot[1].TbBonus, "Barracudaの雷装ボーナスはSwordfishにつかない");
                Assert.AreEqual(3, ship.Slot[2].TbBonus, "Barracudaの雷装ボーナスはBarracudaにつく");
                Assert.AreEqual(66, ship.NightBattlePower, "改修値はSwordfishのみ、雷装ボーナスは夜間機同様に有効");
            }

            [TestMethod]
            public void アークロイヤル改にSwordfish0機と15_5cm三連装副砲2スロずつ()
            {
                var ship = new ShipStatus
                {
                    Firepower = 50 + 2 + 2 + 7 + 7,
                    Torpedo = 3 + 3,
                    ImprovedFirepower = 50,
                    Spec = new ShipSpec
                    {
                        Id = 393,
                        ShipType = 11,
                        ShipClass = 78,
                        Name = "Ark Royal改",
                        Yomi = "アークロイヤル",
                        MinFirepower = 0
                    },
                    Slot = new[]
                    {
                        Swordfish(0), Swordfish(0),
                        _15_5cm三連装副砲,
                        _15_5cm三連装副砲
                    }
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }

            [TestMethod]
            public void アークロイヤル改に九七式艦攻と15_5cm三連装副砲2スロずつ()
            {
                var ship = new ShipStatus
                {
                    Firepower = 50 + 7 + 7,
                    Torpedo = 5 + 5,
                    ImprovedFirepower = 50,
                    Spec = new ShipSpec
                    {
                        Id = 393,
                        ShipType = 11,
                        ShipClass = 78,
                        Name = "Ark Royal改",
                        Yomi = "アークロイヤル",
                        MinFirepower = 0
                    },
                    Slot = new[]
                    {
                        流星改(24), 流星改(30),
                        _15_5cm三連装副砲,
                        _15_5cm三連装副砲
                    }
                };
                ship.ResolveTbBonus(additionalData);
                Assert.AreEqual(0, ship.NightBattlePower);
            }
        }

        [TestClass]
        public class AntiSubmarine
        {
            [TestMethod]
            public void 軽空母()
            {
                var ship = new ShipStatus
                {
                    Level = 99,
                    MaxAsw = 47,
                    Spec = new ShipSpec
                    {
                        ShipType = 7
                    },
                    ShownAsw = 47 + 11,
                    Slot = new[]
                    {
                        水中聴音機零式
                    }
                };
                Assert.IsFalse(ship.EnableAsw, "艦載機なし");

                ship.ShownAsw = 47 + 18;
                ship.Slot = new[]
                {
                    九七式艦攻九三一空(0),
                    水中聴音機零式
                };
                Assert.IsFalse(ship.EnableAsw, "艦載機0機");

                ship.Slot = new[]
                {
                    九七式艦攻九三一空(1),
                    水中聴音機零式
                };
                Assert.IsTrue(ship.EnableAsw, "艦載機あり");
                Assert.AreEqual("48.7", ship.EffectiveAsw.ToString("f1"), "艦載機あり");
            }

            [TestMethod]
            public void 水上機母艦()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec
                    {
                        ShipType = 16
                    },
                    ShownAsw = 10,
                    Slot = new[]
                    {
                        三式水中探信儀
                    }
                };
                Assert.IsFalse(ship.EnableAsw, "艦載機なし");

                ship.ShownAsw = 19;
                ship.Slot = new[]
                {
                    三式水中探信儀,
                    カ号観測機(0)
                };
                Assert.IsFalse(ship.EnableAsw, "艦載機0機");

                ship.Slot = new[]
                {
                    三式水中探信儀,
                    カ号観測機(1)
                };
                Assert.IsTrue(ship.EnableAsw, "艦載機あり");
                Assert.AreEqual("36.5", ship.EffectiveAsw.ToString("f1"));
            }

            [TestMethod]
            public void 速吸改()
            {
                var ship = new ShipStatus
                {
                    Level = 99,
                    MaxAsw = 36,
                    Spec = new ShipSpec
                    {
                        Id = 352,
                        ShipType = 22,
                        GetMinAsw = (Func<int>)(() => 12)
                    },
                    ShownAsw = 36,
                    Slot = new[]
                    {
                        Re2001G改
                    }
                };
                Assert.IsFalse(ship.EnableAsw, "Re2001G改のみで対潜不可");

                ship.Slot = new[]
                {
                    Re2001G改,
                    流星改(0)
                };
                Assert.IsFalse(ship.EnableAsw, "Re2001G改 + 流星改0機");

                ship.Slot = new[]
                {
                    Re2001G改,
                    流星改(3)
                };
                Assert.IsTrue(ship.EnableAsw, "Re2001G改 + 流星改");

                ship.Slot = new[]
                {
                    Re2001G改,
                    瑞雲(0)
                };
                Assert.IsFalse(ship.EnableAsw, "Re2001G改 + 瑞雲0機");

                ship.Slot = new[]
                {
                    Re2001G改,
                    瑞雲(1)
                };
                Assert.IsTrue(ship.EnableAsw, "Re2001G改 + 瑞雲");

                ship.Slot = new[]
                {
                    Re2001G改,
                    カ号観測機(0)
                };
                Assert.IsFalse(ship.EnableAsw, "Re2001G改 + カ号観測機0機");

                ship.Slot = new[]
                {
                    Re2001G改,
                    カ号観測機(1)
                };
                Assert.IsTrue(ship.EnableAsw, "Re2001G改 + カ号観測機");

                ship.Slot = new ItemStatus[0];
                Assert.IsTrue(ship.EnableAsw, "無装備");

                ship.Slot = new[]
                {
                    流星改(0)
                };
                Assert.IsFalse(ship.EnableAsw, "流星改0機");

                ship.Slot = new[]
                {
                    流星改(6)
                };
                Assert.IsTrue(ship.EnableAsw, "流星改");

                ship.Slot = new[]
                {
                    瑞雲(0)
                };
                Assert.IsFalse(ship.EnableAsw, "瑞雲0機");

                ship.Slot = new[]
                {
                    瑞雲(1)
                };
                Assert.IsTrue(ship.EnableAsw, "瑞雲");

                ship.Slot = new[]
                {
                    カ号観測機(0)
                };
                Assert.IsFalse(ship.EnableAsw, "カ号観測機0機");

                ship.Slot = new[]
                {
                    カ号観測機(1)
                };
                Assert.IsTrue(ship.EnableAsw, "カ号観測機");

                ship.Slot = new[]
                {
                    流星改(0),
                    瑞雲(1)
                };
                Assert.IsFalse(ship.EnableAsw, "流星改0機＋瑞雲");

                ship.Slot = new[]
                {
                    流星改(0),
                    カ号観測機(1)
                };
                Assert.IsFalse(ship.EnableAsw, "流星改0機＋カ号観測機");
            }

            [TestMethod]
            public void 対潜装備一つ()
            {
                var ship = new ShipStatus
                {
                    Level = 99,
                    MaxAsw = 63,
                    Spec = new ShipSpec
                    {
                        ShipType = 2
                    },
                    ShownAsw = 63 + 10,
                    Slot = new[]
                    {
                        三式水中探信儀
                    }
                };
                Assert.AreEqual("43.9", ship.EffectiveAsw.ToString("f1"));

                ship.ShownAsw = 63 + 8;
                ship.Slot = new[]
                {
                    三式爆雷投射機
                };
                Assert.AreEqual("40.9", ship.EffectiveAsw.ToString("f1"));

                ship.ShownAsw = 63 + 4;
                ship.Slot = new[]
                {
                    九五式爆雷
                };
                Assert.AreEqual("34.9", ship.EffectiveAsw.ToString("f1"));
            }

            [TestMethod]
            public void 爆雷投射機と爆雷()
            {
                var ship = new ShipStatus
                {
                    Level = 99,
                    MaxAsw = 63,
                    Spec = new ShipSpec
                    {
                        ShipType = 2
                    },
                    ShownAsw = 63 + 12,
                    Slot = new[]
                    {
                        三式爆雷投射機,
                        九五式爆雷
                    }
                };
                Assert.AreEqual("51.6", ship.EffectiveAsw.ToString("f1"));
            }

            [TestMethod]
            public void ソナーとそれ以外()
            {
                var ship = new ShipStatus
                {
                    Level = 99,
                    MaxAsw = 63,
                    Spec = new ShipSpec
                    {
                        ShipType = 2
                    },
                    ShownAsw = 63 + 18,
                    Slot = new[]
                    {
                        三式水中探信儀,
                        三式爆雷投射機
                    }
                };
                Assert.AreEqual("64.3", ship.EffectiveAsw.ToString("f1"));

                ship.ShownAsw = 63 + 14;
                ship.Slot = new[]
                {
                    三式水中探信儀,
                    九五式爆雷
                };
                Assert.AreEqual("57.4", ship.EffectiveAsw.ToString("f1"));

                ship.ShownAsw = 63 + 13;
                ship.Slot = new[]
                {
                    三式水中探信儀,
                    二式12cm迫撃砲改
                };
                Assert.AreEqual("55.6", ship.EffectiveAsw.ToString("f1"));
            }

            [TestMethod]
            public void 三種コンビネーション()
            {
                var ship = new ShipStatus
                {
                    Level = 99,
                    MaxAsw = 63,
                    Spec = new ShipSpec
                    {
                        ShipType = 2
                    },
                    ShownAsw = 63 + 22,
                    Slot = new[]
                    {
                        三式水中探信儀,
                        三式爆雷投射機,
                        九五式爆雷
                    }
                };
                Assert.AreEqual("88.9", ship.EffectiveAsw.ToString("f1"));

                ship.ShownAsw = 63 + 17;
                ship.Slot = new[]
                {
                    三式水中探信儀,
                    二式12cm迫撃砲改,
                    九五式爆雷
                };
                Assert.AreEqual("62.5", ship.EffectiveAsw.ToString("f1"), "三種コンビネーションにならない");
            }

            [TestMethod]
            public void Lvから素対潜を計算()
            {
                var ship = new ShipStatus
                {
                    Level = 37,
                    MaxAsw = 81,
                    Spec = new ShipSpec
                    {
                        Id = 714,
                        ShipType = 1,
                        Name = "昭南改",
                    }
                };
                ship.Spec.GetMinAsw = () => additionalData.MinAsw(ship.Spec.Id);
                Assert.IsFalse(ship.UnresolvedAsw);
                Assert.AreEqual(ship.RawAsw, 54);

                ship.Level = 41;
                Assert.AreEqual(ship.RawAsw, 55);
                ship.Level++;
                Assert.AreEqual(ship.RawAsw, 56);
            }

            [TestMethod]
            public void 装備ボーナス()
            {
                var ship = new ShipStatus
                {
                    Level = 99,
                    MaxAsw = 84,
                    Spec = new ShipSpec
                    {
                        Id = 662,
                        ShipType = 3,
                        Name = "能代改二",
                        GetMinAsw = (Func<int>)(() => 32)
                    },
                    ShownAsw = 84 + 7 + 3,
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 238,
                                Name = "零式水上偵察機11型乙",
                                Type = 10,
                                IconType = 10,
                                SubType = 2,
                                Asw = 7
                            },
                            OnSlot = 1
                        }
                    }
                };
                Assert.AreEqual(84, ship.RawAsw);
                Assert.AreEqual("35.8", ship.EffectiveAsw.ToString("f1"));
            }

            [TestMethod]
            public void 対潜改修()
            {
                var ship = new ShipStatus
                {
                    Level = 99,
                    MaxAsw = 0,
                    Spec = new ShipSpec
                    {
                        Id = 555,
                        ShipType = 7,
                        Name = "瑞鳳改二",
                        GetMinAsw = (Func<int>)(() => -1)
                    },
                    ShownAsw = 10,
                    Slot = new[]
                    {
                        new ItemStatus
                        {
                            Id = 1,
                            Spec = new ItemSpec
                            {
                                Id = 244,
                                Name = "Swordfish Mk.III(熟練)",
                                Type = 8,
                                IconType = 8,
                                SubType = 28,
                                Firepower = 4,
                                Torpedo = 8,
                                Asw = 10
                            },
                            OnSlot = 21
                        }
                    }
                };
                Assert.AreEqual(0, ship.RawAsw);
                Assert.AreEqual("23.0", ship.EffectiveAsw.ToString("f1"));

                ship.ImprovedAsw = 9;
                ship.ShownAsw += ship.ImprovedAsw;
                Assert.AreEqual(9, ship.RawAsw);
                Assert.AreEqual("29.0", ship.EffectiveAsw.ToString("f1"));
            }

            [TestMethod]
            public void 対潜最小値不明な新艦()
            {
                var ship = new ShipStatus
                {
                    Level = 84,
                    MaxAsw = 86,
                    Spec = new ShipSpec
                    {
                        Id = 58800,
                        ShipType = 2,
                        Name = "山風改二",
                    },
                    ShownAsw = 77 + 10 + 3,
                    Slot = new[]{三式水中探信儀}
                };
                ship.Spec.GetMinAsw = () => additionalData.MinAsw(ship.Spec.Id);
                Assert.IsTrue(ship.UnresolvedAsw);
                Assert.AreEqual(80, ship.RawAsw);
                Assert.AreEqual("45.9", ship.EffectiveAsw.ToString("f1"));

                ship.Spec.Id = 588;
                Assert.IsFalse(ship.UnresolvedAsw);
                Assert.AreEqual(77, ship.RawAsw);
                Assert.AreEqual("50.0", ship.EffectiveAsw.ToString("f1"));
            }
        }

        [TestClass]
        public class LoS
        {
            [TestMethod]
            public void アメリカ艦にSGレーダー初期型()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec{ShipClass = 65, Yomi = "アイオワ"},
                    LoS = 71 + 8 + 4 + 8 + 4,
                    Slot = new[]{SGレーダー初期型, SGレーダー初期型}
                };
                Assert.AreEqual(71, ship.RawLoS, "SGレーダー初期型の索敵ボーナスは無視される");
            }

            [TestMethod]
            public void アメリカ艦にSKレーダー()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec{ShipClass = 65, Yomi = "アイオワ"},
                    LoS = 71 + 10 + 1 + 10 + 1,
                    Slot = new[]{SKレーダー, SKレーダー}
                };
                Assert.AreEqual(73, ship.RawLoS, "SGレーダー初期型以外の索敵ボーナスは加算される");
            }

            [TestMethod]
            public void 丹陽雪風改二にSGレーダー初期型()
            {
                var ship = new ShipStatus
                {
                    Spec = new ShipSpec{Id = 656},
                    LoS = 48 + 8 + 3 + 8 + 0,
                    Slot = new[]{SGレーダー初期型, SGレーダー初期型}
                };
                Assert.AreEqual(48, ship.RawLoS, "2個目以降のSGレーダー初期型の索敵ボーナス");
            }
        }

        // ReSharper disable once InconsistentNaming
        private static readonly ItemStatus A12cm30連装噴進砲改二 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 274,
                Type = 21,
                IconType = 15,
                AntiAir = 8
            }
        };

        // ReSharper disable once InconsistentNaming
        private static readonly ItemStatus A25mm三連装機銃集中配備 = new ItemStatus
        {
            Id = 1,
            Spec = new ItemSpec
            {
                Id = 131,
                Type = 21,
                IconType = 15,
                AntiAir = 9
            }
        };

        [TestClass]
        public class AntiAirPropellantBarrageChance
        {
            private ShipStatus _ship;

            [TestInitialize]
            public void Initialize()
            {
                _ship = new ShipStatus
                {
                    AntiAir = 93,
                    Lucky = 46,
                    Spec = new ShipSpec
                    {
                        ShipType = 4
                    },
                    Slot = new ItemStatus[0]
                };
            }

            [TestMethod]
            public void 噴進砲改二なし()
            {
                Assert.AreEqual(0, _ship.AntiAirPropellantBarrageChance);
            }

            [TestMethod]
            public void 噴進砲改二1つ()
            {
                _ship.AntiAir = 85 + 8;
                _ship.Slot = new[]
                {
                    A12cm30連装噴進砲改二
                };
                Assert.AreEqual("61.7", _ship.AntiAirPropellantBarrageChance.ToString("f1"));
            }

            [TestMethod]
            public void 補強増設に噴進砲改二()
            {
                _ship.AntiAir = 85 + 8;
                _ship.SlotEx = A12cm30連装噴進砲改二;
                Assert.AreEqual("61.7", _ship.AntiAirPropellantBarrageChance.ToString("f1"));
            }

            [TestMethod]
            public void 噴進砲改二2つ()
            {
                _ship.AntiAir = 85 + 16;
                _ship.Slot = new[]
                {
                    A12cm30連装噴進砲改二,
                    A12cm30連装噴進砲改二
                };
                Assert.AreEqual("93.8", _ship.AntiAirPropellantBarrageChance.ToString("f1"));
            }

            [TestMethod]
            public void 噴進砲改二2つと機銃()
            {
                _ship.AntiAir = 85 + 25;
                _ship.Slot = new[]
                {
                    A12cm30連装噴進砲改二,
                    A12cm30連装噴進砲改二,
                    A25mm三連装機銃集中配備
                };
                Assert.AreEqual("113.0", _ship.AntiAirPropellantBarrageChance.ToString("f1"), "噴進砲改二2+機銃");
            }

            [TestMethod]
            public void 伊勢型()
            {
                _ship.AntiAir = 85 + 8;
                _ship.Slot = new[]
                {
                    A12cm30連装噴進砲改二
                };
                _ship.Spec.ShipClass = 2;
                Assert.AreEqual("86.7", _ship.AntiAirPropellantBarrageChance.ToString("f1"));
            }
        }
    }
}