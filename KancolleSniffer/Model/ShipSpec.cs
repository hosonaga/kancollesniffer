﻿// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;

namespace KancolleSniffer.Model
{
    public class ShipSpec
    {
        public int Id { get; set; }
        public int SortId { get; set; }
        public string Name { get; set; }
        public string Yomi { get; set; }
        public int MinFirepower { get; set; }
        public int MinTorpedo { get; set; }
        public int FuelMax { get; set; }
        public int BullMax { get; set; }
        public int SlotNum { get; set; }
        public Func<int> GetMinAsw { get; set; }
        public int MinAsw => GetMinAsw?.Invoke() ?? -1;
        public Func<int[]> GetMaxEq { get; set; }
        public int[] MaxEq => GetMaxEq?.Invoke();
        public Func<int> GetNumEquips { get; set; }
        public Action<int> SetNumEquips { get; set; }

        public int NumEquips
        {
            get => GetNumEquips();
            set => SetNumEquips(value);
        }

        public int ShipType { get; set; }
        public int ShipClass { get; set; }
        public string ShipTypeName { get; set; }
        public Dictionary<int, int> UpgradeItems { get; set; } = new Dictionary<int, int>();
        public RemodelInfo Remodel { get; } = new RemodelInfo();

        public class RemodelInfo
        {
            public int Level { get; set; }
            public int After { get; set; }
            public int Base { get; set; } // 艦隊晒しページ用
            public int Step { get; set; } // 同上
        }

        public ShipSpec()
        {
            Id = -1;
            Name = "";
        }

        public double RepairWeight
        {
            get
            {
                switch (ShipType)
                {
                    case  1: // 海防艦
                    case 13: // 潜水艦
                        return 0.5;
                    case  2: // 駆逐艦
                    case  3: // 軽巡洋艦
                    case  4: // 重雷装巡洋艦
                    case 14: // 潜水空母
                    case 16: // 水上機母艦
                    case 17: // 揚陸艦
                    case 21: // 練習巡洋艦
                    case 22: // 補給艦
                        return 1.0;
                    case  5: // 重巡洋艦
                    case  6: // 航空巡洋艦
                    case  7: // 軽空母
                    case  8: // 高速戦艦
                    case 20: // 潜水母艦
                        return 1.5;
                    case  9: // 低速戦艦
                    case 10: // 航空戦艦
                    case 11: // 正規空母
                    case 18: // 装甲空母
                    case 19: // 工作艦
                        return 2.0;
                    default:
                        return 1.0;
                }
            }
        }

        public double TransportPoint
        {
            get
            {
                switch (ShipType)
                {
                    case  2: // 駆逐艦
                        return 5.0;
                    case  3: // 軽巡洋艦
                        return Id == 487 ? 10.0 : 2.0; // 鬼怒改二は大発分を加算
                    case  6: // 航空巡洋艦
                        return 4.0;
                    case 10: // 航空戦艦
                        return 7.0;
                    case 16: // 水上機母艦
                        return 9.0;
                    case 17: // 揚陸艦
                        return 12.0;
                    case 20: // 潜水母艦
                        return 7.0;
                    case 21: // 練習巡洋艦
                        return 6.0;
                    case 22: // 補給艦
                        return 15.0;
                    default:
                        return 0;
                }
            }
        }

        public bool IsSubmarine => ShipType == 13 || ShipType == 14;

        public bool IsBattleship => ShipType == 8 || ShipType == 9 || ShipType == 10;

        public bool IsAircraftCarrier => ShipType == 7 || ShipType == 11 || ShipType == 18;

        public bool IsLightCruiserClass => ShipType == 3 || ShipType == 4 || ShipType == 21;

        public bool IsTrainingCruiser => ShipType == 21;

        public bool IsRepairShip => ShipType == 19;

        public int UpgradeLevel => SortId % 10; // 一の位は改造段階とみなせそう

        public bool EnableNightShelling => IsAircraftCarrier ? MinFirepower > 9 : (MinFirepower + MinTorpedo) > 0;

        public bool IsNightAircraftCarrier =>
            Id == 545 || // Saratoga Mk.II
            Id == 599 || // 赤城改二戊
            Id == 610 || // 加賀改二戊
            Id == 883;   // 龍鳳改二戊

        public bool IsAswCruiserDestroyer =>
            Id == 141 || // 五十鈴改二
            Id == 478 || // 龍田改二
            Id == 624 || // 夕張改二丁
            ShipClass == 91 || // Fletcher級
            // J級改 John C.Butler級改
            (ShipClass == 82 || ShipClass == 87) && UpgradeLevel > 1;

        public bool IsAswAircraftCarrier =>
            Id == 646 || // 加賀改二護
            // 大鷹型改 sort_idがズレてるので初期対潜で代用
            ShipClass == 76 && MinAsw > 59;

        public int[] SgRadarLoSBonus
        {
            get
            {
                if (Id == 651 || Id == 656) // 丹陽、雪風改二
                    return new int[] { 3, 0 };

                switch (ShipClass)
                {
                    case  93: // コロラド
                    case 107: // ワシントン
                    case 102: // サウスダコタ
                    case  65: // アイオワ
                    case  69: // サラトガ
                    case  84: // イントレピッド
                    case  83: // ガンビア・ベイ
                    case 105: // ホーネット
                    case  95: // ノーザンプトン級
                    case 110: // ホノルル
                    case 106: // ヘレナ
                    case  99: // アトランタ
                    case  91: // フレッチャー級
                    case  87: // サミュエル・B・ロバーツ
                        return new int[] { 4 };
                    default:
                        return new int[] {};
                }
            }
        }
    }
}