﻿// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Linq;

namespace KancolleSniffer.Model
{
    public class ItemStatus
    {
        public int Id { get; set; }
        public bool Empty => Id == -1;
        public bool Unimplemented => Id == 0; // 補強増設未開放
        public bool Present => Id > 0;
        public ItemSpec Spec { get; set; } = new ItemSpec();
        public int Level { get; set; }
        public int Alv { get; set; }
        public bool Locked { get; set; }
        public int OnSlot { get; set; }
        public int MaxEq { get; set; }
        public int TbBonus { get; set; }
        public ShipStatus Holder { get; set; }

        public ItemStatus(int id = -1)
        {
            Id = id;
        }

        public ItemStatus FillAirBaseOnSlotMaxEq(int onSlot, int maxEq)
        {
            OnSlot = onSlot;
            MaxEq = maxEq;
            TbBonus = 0;
            return this;
        }

        public bool HasNoneAircraft => OnSlot < 1 && Spec.IsAircraft;

        public bool HasAnyBomber => OnSlot > 0 && Spec.IsAnyBomber;

        public bool HasCarrierBasedBomber => OnSlot > 0 && Spec.IsCarrierBasedBomber;

        public bool HasNightAircraft => OnSlot > 0 && Spec.IsNightAircraft;

        public bool HasNightAircraftSub => OnSlot > 0 && Spec.IsNightAircraftSub;

        public bool HasSwordfishTorpedoBomber => OnSlot > 0 && Spec.IsSwordfishTorpedoBomber;

        public bool HasAswEnabledBomber => OnSlot > 0 && Spec.IsAswEnabledBomber;

        public bool HasAswTorpedoBomber => OnSlot > 0 && Spec.IsAswTorpedoBomber;

        public bool HasAswPatrol => OnSlot > 0 && Spec.IsAswPatrol;

        public bool HasNightRecon => OnSlot > 0 && Spec.IsNightRecon;

        public bool HasAutoGiro => OnSlot > 0 && Spec.IsAutoGiro;

        public bool HasHelicopter => OnSlot > 0 && Spec.IsHelicopter;

        public bool HasFlyingBoat => OnSlot > 0 && Spec.IsFlyingBoat;

        public int[] CalcBomberPower()
        {
            if (!Spec.IsAnyBomber || OnSlot < 1)
                return new int[] {0};

            var basePower = 25 + (Spec.BomberSpec + BomberLevelBonus + TbBonus) * Math.Sqrt(OnSlot);
            if (Spec.IsJetBomber) // 噴式機
            {
                var jetPower = 25 + (Spec.BomberSpec + BomberLevelBonus) * Math.Sqrt(OnSlot);
                return new int[] {(int)jetPower, (int)(basePower / Math.Sqrt(2))};
            }
            if (Spec.IsDiveBomber) // 爆撃
            {
                return new int[] {(int)basePower};
            }
            if (Spec.IsTorpedoBomber) // 雷撃
            {
                return new int[] {(int)(basePower * 0.8), (int)(basePower * 1.5)};
            }

            return new int[0];
        }

        public double CalcNightAircraftPower()
        {
            double a, b;
            if (HasNightAircraftSub) {
                a = 0.0;
                b = 0.3;
            } else if (HasNightAircraft) {
                a = 3.0;
                b = 0.45;
            } else {
                return TbBonus;
            }

            return Spec.Firepower + Spec.Torpedo + TbBonus +
                   a * OnSlot +
                   b * (Spec.Firepower + Spec.Torpedo + Spec.Bomber + Spec.Asw) * Math.Sqrt(OnSlot) +
                   Math.Sqrt(Level);
        }

        public double CalcSwordfishTorpedoPower()
        {
            return (HasSwordfishTorpedoBomber ? Spec.Firepower + Spec.Torpedo + Math.Sqrt(Level) : 0) + TbBonus;
        }

        public Range CalcFighterPower()
        {
            if (!Spec.CanAirCombat || OnSlot < 1)
                return new Range();
            var withoutAlv = (Spec.AntiAir + FighterPowerLevelBonus) * Math.Sqrt(OnSlot);
            return new Range(withoutAlv, AlvBonus);
        }

        private RangeD AlvBonus
        {
            get
            {
                var table = AlvTypeBonusTable;
                if (table == null)
                    return new RangeD();
                return _alvBonus[Alv] + table[Alv];
            }
        }

        public AirCorpsFighterPower.Range CalcFighterPowerInBase()
        {
            if (!Spec.IsAircraft || OnSlot < 1)
                return new AirCorpsFighterPower.Range();
            var withoutAlv =
                (new AirCorpsFighterPower.Pair(Spec.Interception * 1.5, Spec.AntiBomber * 2 + Spec.Interception) +
                 Spec.AntiAir + FighterPowerLevelBonus) * Math.Sqrt(OnSlot);
            return new AirCorpsFighterPower.Range(withoutAlv, AlvBonusInBase);
        }

        public double CalcContactTriggerRate()
        {
            return Spec.ContactTriggerRate * Spec.LoS * Math.Sqrt(OnSlot);
        }

        public double CalcAirReconScore()
        {
            switch (Spec.Type)
            {
                case 10: // 水偵
                case 11: // 水爆
                    return Spec.LoS * Math.Sqrt(Math.Sqrt(OnSlot));
                case 41: // 大艇
                    return Spec.LoS * Math.Sqrt(OnSlot);
                default:
                    return 0;
            }
        }

        private RangeD AlvBonusInBase
        {
            get
            {
                switch (Spec.Type)
                {
                    case  9: // 艦偵
                    case 10: // 水偵
                    case 41: // 大艇
                    case 49: // 陸偵
                        return _alvBonus[Alv];
                    default:
                        return AlvBonus;
                }
            }
        }

        private readonly RangeD[] _alvBonus =
            new[]
            {
                new RangeD(0.0, 0.9), new RangeD(1.0, 2.4), new RangeD(2.5, 3.9), new RangeD(4.0, 5.4),
                new RangeD(5.5, 6.9), new RangeD(7.0, 8.4), new RangeD(8.5, 9.9), new RangeD(10.0, 12.0)
            }.Select(range => range.Sqrt()).ToArray();


        private int[] AlvTypeBonusTable
        {
            get
            {
                switch (Spec.Type)
                {
                    case  6: // 艦戦
                    case 45: // 水戦
                    case 48: // 局地戦闘機
                    case 56: // 噴式戦闘機
                        return new[] {0, 0, 2, 5, 9, 14, 14, 22};
                    case  7: // 艦爆
                    case  8: // 艦攻
                    case 47: // 陸攻
                    case 57: // 噴式戦闘爆撃機
                    case 58: // 噴式攻撃機
                        return new[] {0, 0, 0, 0, 0, 0, 0, 0};
                    case 11: // 水爆
                        return new[] {0, 0, 1, 1, 1, 3, 3, 6};
                    default:
                        return null;
                }
            }
        }

        private double FighterPowerLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  6: // 艦戦
                    case 45: // 水戦
                    case 48: // 陸戦・局戦
                        return 0.2 * Level;
                    case  7: // 爆戦
                        return Spec.IsBakusen ? 0.25 * Level : 0;
                    case 47: // 陸攻
                    case 53: // 重爆
                        return 0.5 * Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        public double EffectiveLoS => (Spec.LoS + LoSLevelBonus) * Spec.LoSScaleFactor;

        private double LoSLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  9: // 艦偵
                    case 10: // 水偵
                    case 41: // 飛行艇
                    case 94: // 艦上偵察機（II）
                        return 1.2 * Math.Sqrt(Level);
                    case 11: // 水爆
                        return 1.15 * Math.Sqrt(Level);
                    case 12: // 小型電探
                        return 1.25 * Math.Sqrt(Level);
                    case 13: // 大型電探
                        return 1.4 * Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        public double FirepowerLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  1: // 小口径
                    case  2: // 中口径
                    case 18: // 三式弾
                    case 19: // 徹甲弾
                    case 21: // 対空機銃
                    case 24: // 上陸用舟艇
                    case 29: // 探照灯
                    case 36: // 高射装置
                    case 37: // 対地噴進砲
                    case 42: // 大型探照灯
                    case 46: // 特型内火艇
                        return Math.Sqrt(Level);
                    case  3: // 大口径
                        return 1.5 * Math.Sqrt(Level);
                    case  4: // 副砲
                        return SecondaryGunLevelBonus;
                    case 14: // ソナー
                    case 40: // 大型ソナー
                    case 15: // 爆雷投射機、その他爆雷
                        return Spec.IsDC ? 0 : 0.75 * Math.Sqrt(Level);
                    case  7: // 艦爆(爆戦以外)
                        return Spec.IsBakusen ? 0 : 0.2 * Level;
                    case  8: // 艦攻
                        return 0.2 * Level;
                    default:
                        return 0;
                }
            }
        }

        private double SecondaryGunLevelBonus
        {
            get
            {
                switch (Spec.Id)
                {
                    case  10: // 12.7cm連装高角砲
                    case  66: // 8cm高角砲
                    case 220: // 8cm高角砲改+増設機銃
                    case 275: // 10cm連装高角砲改+増設機銃
                        return 0.2 * Level;
                    case  12: // 15.5cm三連装副砲
                    case 234: // 15.5cm三連装副砲改
                    case 247: // 15.2cm三連装砲
                        return 0.3 * Level;
                    default:
                        return Math.Sqrt(Level);
                }
            }
        }

        public double TorpedoLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  5: // 魚雷
                    case 21: // 機銃
                        return 1.2 * Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        public double AswLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case 14: // ソナー
                    case 40: // 大型ソナー
                    case 15: // 爆雷
                        return Math.Sqrt(Level);
                    case  7: // 艦爆(爆戦以外)
                        return Spec.IsBakusen ? 0 : 0.2 * Level;
                    case  8: // 艦攻
                        return 0.2 * Level;
                    case 25: // 回転翼機
                        return Spec.Asw > 10 ? 0.3 * Level : 0.2 * Level;
                    default:
                        return 0;
                }
            }
        }

        public double BomberLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  7: // 艦爆(爆戦以外)
                        return Spec.IsBakusen ? 0 : 0.2 * Level;
                    case  8: // 艦攻
                        return 0.2 * Level;
                    case 11: // 水爆
                        return 0.2 * Level;
                    case 47: // 陸攻
                    case 53: // 重爆
                        return Spec.Torpedo > 0 ? 0.7 * Math.Sqrt(Level) : 0;
                    default:
                        return 0;
                }
            }
        }

        public double NightBattleLevelBonus
        {
            get
            {
                switch (Spec.Type)
                {
                    case  1: // 小口径
                    case  2: // 中口径
                    case  3: // 大口径
                    case  5: // 魚雷
                    case 18: // 三式弾
                    case 19: // 徹甲弾
                    case 22: // 特殊潜航艇
                    case 24: // 上陸用舟艇
                    case 29: // 探照灯
                    case 36: // 高射装置
                    case 37: // 対地噴進砲
                    case 42: // 大型探照灯
                    case 46: // 特型内火艇
                        return Math.Sqrt(Level);
                    case  4: // 副砲
                        return SecondaryGunLevelBonus;
                    default:
                        return 0;
                }
            }
        }

        public double EffectiveAntiAirForShip
        {
            get
            {
                switch (Spec.IconType)
                {
                    case 15: // 機銃
                        return 6 * Spec.AntiAir + (Spec.AntiAir > 7 ? 6 : 4) * Math.Sqrt(Level);
                    case 16: // 高角砲
                        return 4 * Spec.AntiAir + (Spec.AntiAir > 7 ? 3 : 2) * Math.Sqrt(Level);
                    case 11: // 電探
                        return 3 * Spec.AntiAir;
                    case 30: // 高射装置
                        return 4 * Spec.AntiAir + 2 * Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        public double EffectiveAntiAirForFleet
        {
            get
            {
                if (Spec.Type == 10) // 水偵
                    return 0.2 * Spec.AntiAir;

                switch (Spec.IconType)
                {
                    case  1: // 小口径
                    case  2: // 中口径
                    case  3: // 大口径
                    case  4: // 副砲
                    case  6: // 艦戦
                    case  7: // 艦爆
                    case 15: // 機銃
                        return (Spec.Id == 9 ? 0.25 : 0.2) * Spec.AntiAir; // id:9 46cm三連装砲
                    case 11: // 電探
                        return 0.4 * Spec.AntiAir + (Spec.AntiAir > 0 ? 1.5 : 0) * Math.Sqrt(Level);
                    case 12: // 三式弾
                        return 0.6 * Spec.AntiAir;
                    case 16: // 高角砲
                        return 0.35 * Spec.AntiAir + (Spec.AntiAir > 7 ? 3 : 2) * Math.Sqrt(Level);
                    case 30: // 高射装置
                        return 0.35 * Spec.AntiAir + 2 * Math.Sqrt(Level);
                    default:
                        return 0;
                }
            }
        }

        // 遠征時の装備改修ボーナスは小数第一位まで有効なため、10倍の値の整数値で管理する
        // 搭載数0の場合は無効のためマイナスする。ボーナス値も無効だが単純には計算できないため、警告を出す
        public int MissionFirepowerLevelBonus
        {
            get
            {
                if (HasNoneAircraft)
                    return Spec.Firepower * -10;

                switch (Spec.Type)
                {
                    case  1: // 小口径
                    case  4: // 副砲
                    case 12: // 小型電探
                    case 19: // 徹甲弾
                    case 21: // 対空機銃
                        return (int)(5 * Math.Sqrt(Level));
                    case  2: // 中口径
                    case  3: // 大口径
                    case 13: // 大型電探
                        return (int)(10 * Math.Sqrt(Level));
                    default:
                        return 0;
                }
            }
        }

        public int MissionAntiAirLevelBonus
        {
            get
            {
                if (HasNoneAircraft)
                    return Spec.AntiAir * -10;

                switch (Spec.IconType)
                {
                    case 15: // 機銃
                    case 16: // 高角砲
                        return (int)(10 * Math.Sqrt(Level));
                    default:
                        return 0;
                }
            }
        }

        public int MissionAswLevelBonus
        {
            get
            {
                if (HasNoneAircraft)
                    return Spec.Asw * -10;

                switch (Spec.Type)
                {
                    case 14: // ソナー
                    case 40: // 大型ソナー
                    case 15: // 爆雷
                        return (int)(10 * Math.Sqrt(Level));
                    case 10: // 水上偵察機
                        return MissionAswAircraft((int)(5 * Math.Sqrt(Level)));
                    case 25: // 回転翼機
                        return MissionAswAircraft((int)(10 * Math.Sqrt(Level)));
                    case  6: // 艦戦
                    case  7: // 艦爆
                    case  8: // 艦攻
                    case  9: // 艦偵
                    case 11: // 水爆
                    case 26: // 対潜哨戒機
                    case 41: // 飛行艇
                    case 45: // 水戦
                    case 57: // 噴式戦闘爆撃機
                    case 58: // 噴式攻撃機
                    case 94: // 艦上偵察機（II）
                        return MissionAswAircraft(0);
                    default:
                        return 0;
                }
            }
        }

        public int MissionAswAircraft(int levelBonus)
        {
            var aws = Spec.Asw * 10;
            if (aws < 1)
                return -aws;

            // TODO: 熟練度補正込みの計算式が確定したら反映する
            var multiplier = (0.65 + Math.Sqrt(Math.Max(OnSlot - 2, 0)) * 0.1);
            return (int)((aws + levelBonus) * multiplier) - aws;
        }

        public int MissionLoSLevelBonus
        {
            get
            {
                if (HasNoneAircraft)
                    return Spec.LoS * -10;

                switch (Spec.Type)
                {
                    case 10: // 水上偵察機
                    case 12: // 小型電探
                    case 13: // 大型電探
                        return (int)(10 * Math.Sqrt(Level));
                    default:
                        return 0;
                }
            }
        }
    }
}