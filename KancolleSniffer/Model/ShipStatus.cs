﻿// Copyright (C) 2017 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using static System.Math;

namespace KancolleSniffer.Model
{
    public class ShipStatus : ICloneable
    {
        public int Id { get; set; }
        public bool Empty => Id == -1;
        public bool Unopened => Id == -2; // 購入済み未開封アイテムに設定した仮の装備艦
        public Fleet Fleet { get; set; }
        public int DeckIndex { get; set; }
        public ShipSpec Spec { get; set; }

        public string Name => Spec.Name;

        public int Level { get; set; }
        public int ExpTotal { get; set; }
        public int ExpToNext { get; set; }
        public int ExpToNextPercent { get; set; }
        public int MaxHp { get; set; }
        public int NowHp { get; set; }
        public int Speed { get; set; }
        public int Cond { get; set; }
        public int Fuel { get; set; }
        public int Bull { get; set; }
        public int[] NdockItem { get; set; }
        public int LoS { get; set; }
        public int Firepower { get; set; }
        public int Torpedo { get; set; }
        public int ShownAsw { get; set; }
        public int MaxAsw { get; set; }
        public int AntiAir { get; set; }
        public int Lucky { get; set; }
        public int ImprovedFirepower { get; set; }
        public int ImprovedTorpedo { get; set; }
        public int ImprovedAntiAir { get; set; }
        public int ImprovedArmor { get; set; }
        public int ImprovedLucky { get; set; }
        public int ImprovedMaxHp { get; set; }
        public int ImprovedAsw { get; set; }
        public bool Locked { get; set; }
        public int SallyArea { get; set; }
        public bool Escaped { get; set; }
        public SpecialAttackStatus SpecialAttack { get; set; } = new SpecialAttackStatus();

        public Damage DamageLevel => CalcDamage(NowHp, MaxHp);

        private IList<ItemStatus> _slot;
        private ItemStatus _slotEx;
        public Func<ItemStatus, ItemStatus> GetItem { get; set; } = item => item;

        public IReadOnlyList<ItemStatus> Slot
        {
            get => _slot.Select(item => GetItem(item)).ToArray();
            set => _slot = value.ToArray();
        }

        public ItemStatus SlotEx
        {
            get => GetItem(_slotEx);
            set => _slotEx = value;
        }

        public void FreeSlot(int idx) => _slot[idx] = new ItemStatus();

        public IEnumerable<ItemStatus> AllSlot => SlotEx.Id == 0 ? Slot : Slot.Concat(new[] {SlotEx});

        public ShipStatus(int id = -1)
        {
            Id = id;
            Spec = new ShipSpec();
            Slot = new ItemStatus[0];
            SlotEx = new ItemStatus(0);
        }

        public ShipStatus FillOnSlotMaxEq(int[] onSlot)
        {
            var index = 0;
            foreach(var slot in Slot)
            {
                slot.OnSlot = onSlot?.ElementAtOrDefault(index) ?? 0;
                slot.MaxEq = slot.Spec.IsFlyingBoat ? 1 : Spec.MaxEq?.ElementAtOrDefault(index) ?? 0;
                index++;
            }
            return this;
        }

        public ShipStatus ResolveTbBonus(AdditionalData additionalData)
        {
            foreach(var slot in Slot)
            {
                slot.TbBonus = 0;
            }

            var item = Slot.Where(slot => slot.HasAnyBomber).OrderByDescending(slot => slot.Spec.BomberSpec).ThenByDescending(slot => slot.OnSlot).FirstOrDefault();
            if (item != null)
                item.TbBonus = additionalData.TbBonus(Spec.Id, Slot.Select(slot => slot.Spec.Id).ToArray());
            return this;
        }

        public enum Damage
        {
            Minor,
            Small,
            Half,
            Badly,
            Sunk
        }

        public void RepairShip()
        {
            NowHp = MaxHp;
            Cond = Max(40, Cond);
        }

        public static Damage CalcDamage(int now, int max)
        {
            if (now == 0 && max > 0)
                return Damage.Sunk;
            var ratio = max == 0 ? 1 : (double)now / max;
            return ratio > 0.75 ? Damage.Minor :
                ratio > 0.5 ? Damage.Small :
                ratio > 0.25 ? Damage.Half : Damage.Badly;
        }

        public TimeSpan RepairTime => TimeSpan.FromSeconds((int)(RepairTimePerHp.TotalSeconds * (MaxHp - NowHp)) + 30);

        public TimeSpan RepairTimePerHp =>
            TimeSpan.FromSeconds(Spec.RepairWeight *
                                 (Level < 12
                                     ? Level * 10
                                     : Level * 5 + Floor(Sqrt(Level - 11)) * 10 + 50));

        // 速吸改(艦攻装備、0機でも)
        private bool IsRyuseiAttack => Spec.Id == 352 && Slot.Any(item => item.Spec.IsCarrierBasedBomber);

        // 速吸改(対潜可能機装備、0機でも)
        private bool IsRyuseiAsw => Spec.Id == 352 && Slot.Any(item => item.Spec.IsAnyBomber || item.Spec.IsAswPatrol);

        public double EffectiveFirepower
        {
            get
            {
                if (Spec.IsSubmarine)
                    return 0;

                if (Spec.IsAircraftCarrier || IsRyuseiAttack)
                    return EffectiveAircraftCarrierFirepower;

                return Firepower + AllSlot.Sum(item => item.FirepowerLevelBonus) + Fleet.CombinedFirepowerBonus + 5 + ShipTypeGunTypeFirepower;
            }
        }

        public double EffectiveAircraftCarrierFirepower
        {
            get
            {
                if (!Slot.Any(item => item.HasCarrierBasedBomber))
                    return 0;

                var specs = (from item in Slot where item.Spec.IsAircraft select item.Spec).ToArray();
                var torpedo = specs.Sum(s => s.Torpedo);
                var bomber = specs.Sum(s => s.Bomber);
                if (torpedo + bomber < 1)
                    return 0;

                return (int)((Firepower + torpedo + AllSlot.Sum(item => item.FirepowerLevelBonus + item.TbBonus) +
                              (int)(bomber * 1.3) + Fleet.CombinedFirepowerBonus) * 1.5) + 55;
            }
        }

        public double ShipTypeGunTypeFirepower
        {
            get
            {
                if (Spec.IsLightCruiserClass)
                    return Sqrt(AllSlot.Count(item => item.Spec.IsLightCruiserGun1)) + Sqrt(AllSlot.Count(item => item.Spec.IsLightCruiserGun2)) * 2;

                if (Spec.ShipClass == 64)
                    return Sqrt(AllSlot.Count(item => item.Spec.IsItalyHeavyCruiserGun));

                return 0;
            }
        }

        public double EffectiveTorpedo
        {
            get
            {
                if (Spec.MinTorpedo + ImprovedTorpedo > 0)
                    return Torpedo + AllSlot.Sum(item => item.TorpedoLevelBonus) + Fleet.CombinedTorpedoPenalty + 5;

                return 0;
            }
        }

        public double EffectiveAsw
        {
            get
            {
                var rawAsw = RawAsw;
                // 表示対潜から素対潜と対潜攻撃力に計算されない装備を引くことで、ボーナス込みの装備対潜とする
                var itemAsw = ShownAsw - rawAsw - AllSlot.Sum(item => item.Spec.IneffectiveAsw);
                var sonar = HasSonar;
                var dct = AllSlot.Any(item => item.Spec.IsDCT);
                var dc = AllSlot.Any(item => item.Spec.IsDC);
                var miscDC = AllSlot.Any(item => item.Spec.IsMiscDC);
                var levelBonus = AllSlot.Sum(item => item.AswLevelBonus);

                var bonus = 1.0;
                if (dct && dc)
                    bonus = 1.1;
                if (sonar && (dct || dc || miscDC))
                    bonus = 1.15;
                if (sonar && dct && dc)
                    bonus = 1.15 * 1.25;

                return bonus * (Sqrt(rawAsw) * 2 + itemAsw * 1.5 + levelBonus + EffectiveAswBase);
            }
        }

        public int EffectiveAswBase
        {
            get
            {
                // 加賀改二護、速吸改(対潜可能機装備、0機でも)
                if (Spec.Id == 646 || IsRyuseiAsw)
                    return 8;

                switch (Spec.ShipType)
                {
                    case  1: // 海防艦
                    case  2: // 駆逐
                    case  3: // 軽巡
                    case  4: // 雷巡
                    case 21: // 練巡
                    case 22: // 補給艦
                        return 13;
                    case  6: // 航巡
                    case  7: // 軽空
                    case 10: // 航戦
                    case 16: // 水母
                    case 17: // 揚陸艦
                        return 8;
                    default:
                        return 0;
                }
            }
        }

        public int RawAsw
        {
            get
            {
                // 対潜最大値はapiから取得可能。この値が0なら、素対潜は改修分(瑞鳳改二など)のみと判定
                if (MaxAsw < 1)
                    return ImprovedAsw;

                // 対潜最大値が設定されていて対潜最小値が未定義(新艦など)なら、表示値から装備対潜を引いて仮の素対潜とする
                if (Spec.MinAsw < 0)
                    return ShownAsw - AllSlot.Sum(item => item.Spec.Asw);

                // レベルと最大値最小値から現在の素対潜を計算する
                return (int)((MaxAsw - Spec.MinAsw) * Level / 99.0) + Spec.MinAsw + ImprovedAsw;
            }
        }

        public bool UnresolvedAsw => MaxAsw > 0 && Spec.MinAsw < 0;

        private bool HasSonar => AllSlot.Any(item => item.Spec.IsSonar);

        private bool HasAswEnabledAircraft => Slot.Any(item => item.HasAswEnabledBomber || item.HasAswPatrol || item.HasFlyingBoat);

        public bool EnableAsw
        {
            get
            {
                // 軽空母、加賀改二護、速吸改(艦攻装備、0機でも)
                if (Spec.ShipType == 7 || Spec.Id == 646 || IsRyuseiAttack)
                    return Slot.Any(item => item.HasCarrierBasedBomber) &&
                              (Slot.Any(item => item.HasAswEnabledBomber) ||
                               Slot.Any(item => item.HasAnyBomber) && Slot.Any(item => item.HasAswPatrol));

                // 速吸改(対潜可能機装備、0機でも)
                if (IsRyuseiAsw)
                    return HasAswEnabledAircraft;

                switch (Spec.ShipType)
                {
                    case  1: // 海防艦
                    case  2: // 駆逐
                    case  3: // 軽巡
                    case  4: // 雷巡
                    case 21: // 練巡
                    case 22: // 補給艦
                        return RawAsw > 0;
                    case  6: // 航巡
                    case 10: // 航戦
                    case 16: // 水母
                    case 17: // 揚陸艦
                        return HasAswEnabledAircraft;
                    default:
                        return false;
                }
            }
        }

        public bool EnableOpeningAsw
        {
            get
            {
                if (Spec.IsAswCruiserDestroyer)
                    return true;

                if (Spec.IsAswAircraftCarrier)
                    return HasAswEnabledAircraft;

                if (Spec.Id == 554) // 日向改二
                    return Slot.Count(item => item.HasAutoGiro) > 1 ||
                           Slot.Count(item => item.HasHelicopter) > 0;

                if (Spec.ShipType == 1) // 海防艦
                    return ShownAsw > 74 && Slot.Sum(item => item.Spec.Asw) > 3 ||
                           ShownAsw > 59 && HasSonar;

                if (Spec.ShipType == 7) // 軽空母
                    return HasAswEnabledAircraft &&
                          (ShownAsw > 99 && HasSonar ||
                           ShownAsw > 64 && Slot.Any(item => item.Spec.IsAswTorpedoBomber || item.Spec.IsAswPatrol) ||
                           ShownAsw > 49 && Slot.Any(item => item.Spec.IsAswTorpedoBomber || item.Spec.IsAswPatrol) && HasSonar);

                return ShownAsw > 99 && HasSonar && EnableAsw;
            }
        }

        public double NightBattlePower
        {
            get
            {
                if (Slot.Any(item => item.HasNightAircraft) && // 夜戦か夜攻
                    (Spec.IsNightAircraftCarrier ||            // 夜戦空母
                     Spec.IsAircraftCarrier && AllSlot.Any(item => item.Spec.IsNightOperationAviationPersonnel))) // 空母かつ夜間作戦航空要員
                    return Spec.MinFirepower + ImprovedFirepower + Slot.Sum(item => item.CalcNightAircraftPower());

                // アークロイヤル+Swordfish
                if (Spec.ShipClass == 78 && Slot.Any(item => item.HasSwordfishTorpedoBomber))
                    return Spec.MinFirepower + ImprovedFirepower + Slot.Sum(item => item.CalcSwordfishTorpedoPower());

                if (Spec.EnableNightShelling)
                    return Firepower + Torpedo + AllSlot.Sum(item => item.NightBattleLevelBonus);

                return 0;
            }
        }

        // 遠征時の装備改修ボーナスは小数第一位まで有効なため、10倍の値の整数値で管理する
        public int MissionFirepower => Firepower * 10 + AllSlot.Sum(item => item.MissionFirepowerLevelBonus);

        public int MissionAntiAir => AntiAir * 10 + AllSlot.Sum(item => item.MissionAntiAirLevelBonus);

        public int MissionAsw => ShownAsw * 10 + AllSlot.Sum(item => item.MissionAswLevelBonus);

        public int MissionLoS => LoS * 10 + AllSlot.Sum(item => item.MissionLoSLevelBonus);

        public bool HasNoneAircraft => Slot.Any(item => item.HasNoneAircraft);

        public int PreparedDamageControl =>
            DamageLevel != Damage.Badly
                ? -1
                : SlotEx.Spec.Id == 42 || SlotEx.Spec.Id == 43
                    ? SlotEx.Spec.Id
                    : Slot.FirstOrDefault(item => item.Spec.Id == 42 || item.Spec.Id == 43)?.Spec.Id ?? -1;

        public double TransportPoint
            => Spec.TransportPoint + AllSlot.Sum(item => item.Spec.TransportPoint);

        public int EffectiveAntiAirForShip
        {
            get
            {
                if (AllSlot.All(item => item.Empty || item.Unimplemented))
                    return AntiAir;
                var vanilla = AntiAir - AllSlot.Sum(item => item.Spec.AntiAir);
                var x = vanilla + AllSlot.Sum(item => item.EffectiveAntiAirForShip);
                return (int)(x / 2) * 2;
            }
        }

        public int EffectiveAntiAirForFleet => (int)AllSlot.Sum(item => item.EffectiveAntiAirForFleet);

        public double AntiAirPropellantBarrageChance
        {
            get
            {
                var launcherCount = AllSlot.Count(item => item.Spec.Id == 274);
                if (launcherCount == 0)
                    return 0;
                var iseClass = Spec.ShipClass == 2;
                var baseChance = (EffectiveAntiAirForShip + 0.9 * Lucky) / 281.0;
                return (baseChance + 0.15 * (launcherCount - 1) + (iseClass ? 0.25 : 0)) * 100;
            }
        }

        public int EffectiveFuelMax => Max((int)(Spec.FuelMax * (Level >= 100 ? 0.85 : 1.0)), Empty ? 0 : 1);

        public int EffectiveBullMax => Max((int)(Spec.BullMax * (Level >= 100 ? 0.85 : 1.0)), Empty ? 0 : 1);

        public int RawLoS
        {
            get
            {
                var sgBonus = Spec.SgRadarLoSBonus;
                var sgBonusIndex = 0;
                return LoS - AllSlot.Sum(item =>
                {
                    var los = item.Spec.LoS;
                    if (item.Spec.IsSgRadar)
                    {
                        los += sgBonus.Length > sgBonusIndex ? sgBonus[sgBonusIndex] : sgBonus.LastOrDefault();
                        sgBonusIndex++;
                    }
                    return los;
                });
            }
        }

        public bool EnemyAirCombatUnknown => Spec.MaxEq == null && Slot.Any(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0);

        public bool EnemyInterceptionUnknown => Spec.MaxEq == null && Slot.Any(item => item.Spec.IsAircraft && item.Spec.AntiAir > 0);

        public int EnemyAirCombat => Slot.Where(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0 && item.MaxEq > 0).Sum(item => (int)Floor(item.Spec.AntiAir * Sqrt(item.MaxEq)));

        public int EnemyInterception => Slot.Where(item => item.Spec.IsAircraft && item.Spec.AntiAir > 0 && item.MaxEq > 0).Sum(item => (int)Floor(item.Spec.AntiAir * Sqrt(item.MaxEq)));

        public int EnemyAirCombatAircrafts => Slot.Where(item => item.Spec.CanAirCombat && item.MaxEq > 0).Sum(item => item.MaxEq);

        public int EnemyInterceptionAircrafts => Slot.Where(item => item.Spec.IsAircraft && item.MaxEq > 0).Sum(item => item.MaxEq);

        public bool EnemyHasJetBomber => Slot.Any(item => item.Spec.IsJetBomber);

        public string GetToolTipString()
        {
            var result = new List<string>();
            if (ShipMaster.IsEnemyId(Spec.Id))
            {
                result.Add("id:" + Spec.Id);

                if (Slot.Any(item => item.Spec.IsAircraft))
                {
                    var airCombatText    =    EnemyAirCombatUnknown ? "?" : EnemyAirCombat.ToString();
                    var interceptionText = EnemyInterceptionUnknown ? "?" : EnemyInterception.ToString();
                    result.Add($"制空:{airCombatText} 対基地:{interceptionText}");
                }
            }

            result.AddRange
                (from item in AllSlot
                    where !item.Empty
                    select item.Spec.Name +
                           (item.Spec.IsAircraft
                               ? $" {item.OnSlot}/{item.MaxEq}"
                               : ""));

            return string.Join("\r\n", result);
        }

        public object Clone()
        {
            var r = (ShipStatus)MemberwiseClone();
            r.Slot = r.Slot.ToArray(); // 戦闘中のダメコンの消費が見えないように複製する
            return r;
        }
    }

    public class SpecialAttackStatus
    {
        public bool Fire { get; set; }
        public int Type { get; set; }
        public int Count { get; set; }

        public void Trigger(int type)
        {
            Fire = true;
            Type = type;
            Count++;
        }

        public SpecialAttackStatus After()
        {
            return new SpecialAttackStatus{Fire = false, Type = Type, Count = Count};
        }
    }
}